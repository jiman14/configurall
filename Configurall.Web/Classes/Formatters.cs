﻿using System.Collections.Generic;
using System.Linq;

namespace ConfigurallWeb.Classes
{
    public static class Formatters
    {
        public static string StringListToString(List<string> stringList, string separator=", ")
        => string.Join(separator, stringList ?? (new string[0]).ToList());
    }
}

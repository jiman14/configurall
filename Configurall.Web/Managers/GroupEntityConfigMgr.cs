﻿using Configurall.Connector;
using Configurall.Shared.DTOs;
using ConfigurallWeb.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConfigurallWeb.Managers
{
    public class GroupEntityConfigMgr
    {
        private string CategoryName { get; set; }
        private string GroupName { get; set; }

        public Dictionary<string, DataRow> Rows { get; private set; } = new Dictionary<string, DataRow>();

        private static IEnumerable<ConfigResDTO> Configs { get; set; }

        private void ClearRows()
        {
            var eds = Rows.Values.SelectMany(v => v.EntitiesData.Values);
            foreach (var ed in eds)
            {
                foreach (var component in ed.ComponentsData)
                {
                    component.EntityProp = null;
                    component.EntitySchemaProp = null;
                }
                (ed.ComponentsData as List<ComponentData>).Clear();
            }
            foreach (var row in Rows.Values)
                row.EntitiesData.Clear();

            Rows.Clear();
        }

        public async Task Get(string categoryName, string groupName)
        {
            if (Rows.Count() > 0)
                ClearRows();

            CategoryName = categoryName;
            GroupName = groupName;

            if (categoryName == string.Empty || groupName == string.Empty)
                return;
            
            try
            {
                Configs = await Config.GetConfigUniverse(categoryName, groupName);
                if (Configs == null)
                    return;

                foreach (var config in Configs.Where(c=> c.GroupName == groupName).OrderBy(c => c.RowId).ThenBy(c => c.EntityName))
                {
                    var jEntity = JsonConvert.DeserializeObject<JObject>(config.EntitySchema);
                    var jContent = string.IsNullOrEmpty(config.EntityContent) ? null :
                        JsonConvert.DeserializeObject<JObject>(config.EntityContent);

                    var entityData = new EntityData { 
                        EntityName = config.EntityName, 
                        JEntity = jEntity, 
                        ComponentsData = GetConfigEntity(config, jEntity, jContent) 
                    };

                    if (Rows.ContainsKey(config.RowId))
                    {
                        if (!Rows[config.RowId].EntitiesData.ContainsKey(config.EntityName))
                            Rows[config.RowId].EntitiesData.Add(config.EntityName, entityData);
                    }
                    else
                    {
                        var entitySelect = new SelectData
                        {
                            Label = config.RowId,
                            Name = EntityName(config.EntityName),
                            RowId = config.RowId,
                            EntityName = config.EntityName,
                            Options = Configs.Where(c => c.GroupName == groupName && c.RowId == config.RowId).Select(c => EntityName(c.EntityName)),
                            Value = EntityName(config.EntityName)
                        };
                        entitySelect.Selected += EntityChanged;
                        var dataRow = new DataRow
                        {
                            RowId = config.RowId,
                            EntitySelect = entitySelect
                        };
                        dataRow.EntitiesData.Add(config.EntityName, entityData);
                        Rows.Add(config.RowId, dataRow);
                    }
                }

                foreach (var ed in Rows.Values)
                    ed.EntitiesData.First().Value.Visible = true;

                foreach (var comp in Rows.Values.SelectMany(r => r.EntitiesData).Select(e => e.Value).SelectMany(e => e.ComponentsData))
                    comp.EntitySelected = comp.EntityName;
            }
            catch (Exception exc)
            {
                LogMgr.Error<GroupEntityConfigMgr>(exc);
            }

        }

        public async Task Refresh()
        {
            Rows.Clear();
            await Get(CategoryName, GroupName);
        }
        private void EntityChanged(object sender, EventArgs e)
        {
            try
            {
                var entitySelect = (SelectData)sender;

                foreach (var entity in Rows[entitySelect.RowId].EntitiesData.Values)
                    entity.Visible = entity.EntityName.EndsWith(entitySelect.Value);

                foreach (var comp in Rows[entitySelect.RowId].EntitiesData.Values.SelectMany(e => e.ComponentsData))
                    comp.EntitySelected = entitySelect.Value;
            }
            catch (Exception exc)
            {
                LogMgr.Error<GroupEntityConfigMgr>(exc);
            }
        }
        private string EntityName(string entityName)
        => (entityName.IndexOf(".") > 0) ? entityName.Substring(entityName.LastIndexOf(".") + 1) : entityName;

        private IEnumerable<ComponentData> GetConfigEntity(ConfigResDTO config, JObject jEntity, JObject jContent)
        {
            var components = new List<ComponentData> { };
            try
            {
                foreach (var propEntity in jEntity.Properties().Where(p => p.Name != "$type"))
                {
                    var propEntityTemp = propEntity.DeepClone() as JProperty;
                    GetContentFromParentEntity(config, jContent, propEntity, out JToken value);
                    propEntity.Value = value;

                    ComponentData cd;
                    if (config.EntityCatalogs.Any(e => e.EntityProperty == propEntity.Name))
                        cd = new SelectData { Options = GetCatalog(config, propEntity.Name), Value = propEntity.Value.ToObject<string>() };
                    else if (propEntity.Value.Type == JTokenType.Boolean)
                        cd = new CheckBoxData { Value = propEntity.Value.ToObject<bool>() };
                    else if (propEntity.Value.Type == JTokenType.Array)
                        cd = new TextInputData { PlaceHolder = propEntity.Name, Value = JsonConvert.SerializeObject(propEntity.Value) };
                    else
                        cd = new TextInputData { PlaceHolder = propEntity.Name, Value = propEntity.Value.ToObject<string>() };

                    cd.EntitySchemaProp = propEntityTemp;
                    cd.EntityProp = propEntity;
                    cd.RowId = config.RowId;
                    cd.Name = propEntity.Name;
                    cd.EntityName = config.EntityName;
                    components.Add(cd);
                }
            }
            catch (Exception exc)
            {
                LogMgr.Error<GroupEntityConfigMgr>(exc);
            }
            return components;
        }

        private bool GetContentFromParentEntity(ConfigResDTO config, JObject jContent, JProperty propEntity, out JToken value)
        {
            value = propEntity.Value;
            var propContent = jContent?.Property(propEntity.Name);
            if (propContent != null && propContent.Value.Type != JTokenType.Null &&
                (propEntity.Value.Type == JTokenType.Null || propContent.Value.Type == propEntity.Value.Type))
            {
                value = propContent.Value;
                return true;
            }

            if (config.EntityInherits == null)
                return false;
            
            foreach (var parentEntity in config.EntityInherits)
            {
                var configParent = Configs.FirstOrDefault(c => c.RowId == config.RowId && c.EntityName == parentEntity);
                if (configParent == null || string.IsNullOrEmpty(configParent.EntityContent)) continue;
                
                var jContentParent = JsonConvert.DeserializeObject<JObject>(configParent.EntityContent);

                if (jContentParent != null && jContentParent.Property(propEntity.Name) != null &&
                    GetContentFromParentEntity(configParent, jContentParent, propEntity, out value))
                    return true;
            }
            return false;
        }

        private IEnumerable<string> GetCatalog(ConfigResDTO config, string propName)
        => config.EntityCatalogs.FirstOrDefault(e => e.EntityProperty == propName)?.Catalog;

        public async Task<bool> Save()
        {
            if (CategoryName == string.Empty || GroupName == string.Empty)
                return true;
            try
            {
                bool res = true;
                foreach (var row in Rows.Values)
                    foreach (var entityData in row.EntitiesData.Values)
                        res = res && await Config.SetConfiguration(
                            CategoryName,
                            GroupName,
                            entityData.EntityName,
                            row.RowId,
                            GetEntityData(entityData));
                return res;
            }
            catch (Exception exc)
            {
                LogMgr.Error<GroupEntityConfigMgr>(exc);
                return false;
            }
        }
        private string GetEntityData(EntityData entityData) 
        {
            foreach (var comp in entityData.ComponentsData)
                if (comp.EntityProp.Value.ToString() == comp.EntitySchemaProp.Value.ToString())
                    comp.EntityProp.Value = null;
            return JsonConvert.SerializeObject(entityData.JEntity);
        }

        public async Task<bool> Remove(string rowId, string entityValue)
        {
            if (CategoryName == string.Empty || GroupName == string.Empty)
                return true;
            try
            {
                return await Config.DeleteConfiguration(
                    CategoryName,
                    GroupName,
                    rowId,
                    Rows[rowId].EntitiesData.Values
                                    .FirstOrDefault(e => e.EntityName.EndsWith(entityValue))
                                    .EntityName);
            }
            catch (Exception exc)
            {
                LogMgr.Error<GroupEntityConfigMgr>(exc);
                return false;
            }
        }
    }
}

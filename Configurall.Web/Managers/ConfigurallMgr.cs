﻿using Configurall.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConfigurallWeb.Managers
{
    public static class Connector
    {
        public static Instance Instance { get; private set; }

        public static void Initialize(string configurallApiKey, string configurallURL)
        => Instance = new Instance(configurallApiKey, configurallURL);        

    }
}

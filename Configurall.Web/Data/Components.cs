﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConfigurallWeb.Data
{
    public class DataRow
    {
        public string RowId { get; set; }
        public SelectData EntitySelect { get; set; }
        public Dictionary<string, EntityData> EntitiesData { get; set; } = new Dictionary<string, EntityData>();
    }

    public class EntityData
    {
        public bool Visible { get; set; } = false;
        public string EntityName { get; set; }
        public JObject JEntity { get; set; }
        public IEnumerable<ComponentData> ComponentsData { get; set; } = new List<ComponentData>();
    }
    public class ComponentData
    {
        public virtual string ElementType { get; set; }
        public string EntitySelected { get; set; }
        public string RowId { get; set; }
        public string EntityName { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
        public JProperty EntityProp { get; set; } = new JProperty("temp");
        public JProperty EntitySchemaProp { get; set; } = new JProperty("temp");
    }

    public class LabelData : ComponentData
    {
        public override string ElementType { get => "Label"; }
    }

    public class CheckBoxData : ComponentData
    {
        public override string ElementType { get => "CheckBox"; }
        private bool val;
        public bool Value
        {
            get { return val; }
            set
            {
                if (val != value)
                {
                    val = value;
                    EntityProp.Value = new JValue(val);
                }            
            }
        }
    }
    public class TextInputData : ComponentData
    {
        public override string ElementType { get => "TextInput"; }
        public string PlaceHolder { get; set; }
        private string val;
        public string Value
        {
            get { return val; }
            set
            {
                try
                {
                    if (EntityName?.EndsWith(EntitySelected) ?? true &&
                        val != value)
                    {
                        val = value;
                        if (EntityProp.Value.Type == JTokenType.Array && val.StartsWith("["))
                            EntityProp.Value = JsonConvert.DeserializeObject<JArray>(val);
                        else if (EntityProp.Value.Type == JTokenType.Array)
                            EntityProp.Value = new JArray();
                        else
                            EntityProp.Value = EntityProp.Value.Type switch
                            {
                                JTokenType.Integer => new JValue(int.Parse(val)),
                                JTokenType.Float => new JValue(float.Parse(val)),
                                _ => new JValue(val)
                            };                                
                    }
                }
                catch  (Exception exc)
                {
                    Console.WriteLine(exc.Message);
                }
            }
        }
    }

    public class SelectData : ComponentData
    {
        public EventHandler Selected;
        public override string ElementType { get => "Select"; }
        public IEnumerable<string> Options { get; set; } = new List<string>();
        public JObject Content { get; set; }
        public bool IgnoreValue = false;
        private string val;
        public string Value { 
            get { 
                return val; 
            } 
            set 
            {
                if (!IgnoreValue && val != value)
                {
                    val = value;
                    EntityProp.Value = EntityProp.Value.Type switch
                    {
                        JTokenType.Integer => new JValue(int.Parse(val)),
                        JTokenType.Float => new JValue(float.Parse(val)),
                        _ => new JValue(val)
                    };
                    if (Selected != null)
                        Selected(this, null);
                }
                IgnoreValue = false;
            }
        }
    }

    public class RadioButtonData : ComponentData
    {
        public override string ElementType { get => "RadioButton"; }

        public Dictionary<string, string> Options { get; set; }

    }
}

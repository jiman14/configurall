﻿using Dapper;
using Configurall.Model;
using System;

namespace Configurall.Data
{
    #region Basebuilder enums
    internal enum CommandType
    {
        select,
        insert,
        update,
        delete
    }
    public enum Operator
    {
        Between,
        Distinct,
        Equals,
        In,
        IsNull,
        Like,
        LikeFull,
        EndWith,
        BeginWith,
        GreaterThan,
        GreaterOrEqualsThan,
        LesserThan,
        LesserOrEqualsThan,
        NotLike,
        NotLikeFull,
        NotNull,
        NotEndWith,
        NotBeginWith
    }
    #endregion

    public class BaseBuilder<T>
        where T: BaseModel
    {
        #region Query parts
        internal CommandType commandType { get; set; }
        protected string selectFields { get; set; }
        protected string fromClause { get; set; }
        protected string whereClause { get; set; }
        protected string groupClause { get; set; }
        protected string havingClause { get; set; }
        protected string orderClause { get; set; }
        protected string LimitClause { get; set; }
        protected string deleteQuery { get; set; }
        #endregion

        #region Variables

        BaseModel Model { get; set; }

        private bool WhereKeyWord = false;
        private bool Parenthesis = false;

        internal string Query
        {
            get
            {
                switch (commandType)
                {
                    case CommandType.select: return $"select {selectFields} from {fromClause} {whereClause} {groupClause} {havingClause} {orderClause} {LimitClause}";
                    case CommandType.delete: return $"{deleteQuery} {whereClause}";
                    default: return null;
                }
            }
        }
        #endregion

        #region Initialize
        internal DynamicParameters Parameters { get; set; }
        private int ParameterIndex { get; set; } = 0;
        internal BaseBuilder()
        {
            Model = (BaseModel) Activator.CreateInstance<T>();
            Parameters = new DynamicParameters();
        }
        #endregion

        #region Base query filter

        internal void OpenParenthesis()
        {
            Parenthesis = true;
        }
        internal void CloseParenthesis()
        {
            whereClause += ")";
        }
        internal BaseBuilder<T> Where(bool andJoin, string column, Operator op, params object[] values)
        {
            if ((op == Operator.Like || op == Operator.LikeFull || op == Operator.EndWith || op == Operator.BeginWith) &&
                (values == null || values[0] == null || values[0].ToString() == string.Empty))
                return this;

            if (op == Operator.Between && (values.Length != 2 ||
                values[0] == null || values[0].ToString() == string.Empty ||
                values[1] == null || values[1].ToString() == string.Empty))
                return this;

            var joinSep = (andJoin) ? "and" : "or";

            if (!WhereKeyWord)
            {
                WhereKeyWord = true;
                whereClause += " where ";
                joinSep = "";
            }
            //var com = values.Length > 0 && values[0] is bool ? string.Empty : "'";

            var parenthesis = Parenthesis ? "(" : string.Empty;
            Parenthesis = false;

            var pIndex = ParameterIndex+1;
            var paramNumber = 1;
            var filter = "";
            switch (op)
            {
                case Operator.In:       filter = $"{column} in (@P{pIndex}) "; 
                                        paramNumber = -1; break;
                case Operator.IsNull:   filter = $"{column} is Null "; 
                                        paramNumber = 0; break;
                case Operator.NotNull:  filter = $"{column} is not null "; 
                                        paramNumber = 0; break;
                case Operator.Distinct: filter = $"{column} <> @P{pIndex} "; break;
                case Operator.Equals:   filter = $"{column} = @P{pIndex} "; break;
                case Operator.Like:     filter = $"{column} like @P{pIndex} "; break;
                case Operator.LikeFull: filter = $"{column} like CONCAT('%', @P{pIndex}, '%') "; break;
                case Operator.EndWith:  filter = $"{column} like CONCAT('%', @P{pIndex}) "; break;
                case Operator.BeginWith:filter = $"{column} like CONCAT(@P{pIndex}, '%') "; break;
                case Operator.NotLike:  filter = $"{column} not like @P{pIndex} "; break;
                case Operator.NotLikeFull: 
                                        filter = $"{column} not like CONCAT('%', @P{pIndex}, '%') "; break;
                case Operator.NotEndWith:
                                        filter = $"{column} not like CONCAT('%', @P{pIndex}) "; break;
                case Operator.NotBeginWith: 
                                        filter = $"{column} not like CONCAT(@P{pIndex}, '%') "; break;
                case Operator.GreaterThan:  
                                        filter = $"{column} > @P{pIndex} "; break;
                case Operator.GreaterOrEqualsThan:  
                                        filter = $"{column} >= @P{pIndex} "; break;
                case Operator.LesserThan:   
                                        filter = $"{column} < @P{pIndex} "; break;
                case Operator.LesserOrEqualsThan:   
                                        filter = $"{column} <= @P{pIndex} "; break;
                case Operator.Between:  filter = $"{column} between @P{++pIndex} and @P{pIndex} "; 
                                        paramNumber = 2; break;
            };

            if (paramNumber == -1)
                Parameters.Add($"P{++ParameterIndex}", values, dbType: Model.GetPropertyType<T>(column));

            for (int i = 0; i < paramNumber; i++)
                Parameters.Add($"P{++ParameterIndex}", values[i], dbType: Model.GetPropertyType<T>(column));

            whereClause += $" {joinSep} {parenthesis}({filter})";

            return this;
        }
        #endregion
    }
}
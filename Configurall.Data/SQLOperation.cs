﻿using MySql.Data.MySqlClient;
using System;
using System.Runtime.CompilerServices;

namespace Configurall.Data
{
    public class SQLOperation : SQLConnection
    {
        public static R NonTransaction<R>(Func<SQLCommand, R> func, [CallerMemberName] string callerMemberName="")
        {
            MySqlConnection con = null;
            R res = default(R);
            try
            {
                con = Open();
                if (con == null)
                    return default(R);

                var sqlCommand = new SQLCommand(con);

                res = func.Invoke(sqlCommand);

            }
            catch (MySqlException ex)
            {
                ErrorManager.Log(ex, callerMemberName);
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
            return res;
        }

        public static R Transaction<R>(Func<SQLCommand, R> func, [CallerMemberName] string callerMemberName = "")
        {
            MySqlConnection con = null;
            MySqlTransaction trans = null;
            R res = default(R);
            try
            {
                con = Open();
                if (con == null)
                    return default(R);

                trans = OpenTransaction(con);

                var sqlCommand = new SQLCommand(con, trans);

                res = func.Invoke(sqlCommand);
            }
            catch (MySqlException ex)
            {
                ErrorManager.Log(ex, callerMemberName);
                if (trans != null)
                    Rollback(trans);
            }
            finally
            {
                if (con != null)
                    CloseTransaction(trans, con);
            }
            return res;
        }

        public static bool Rollback(MySqlTransaction mySqlTransaction)
        {
            try
            {
                mySqlTransaction.Rollback();
                return true;
            }
            catch (MySqlException ex)
            {
                ErrorManager.Log(ex);
                return false;
            }
        }

        private static MySqlTransaction OpenTransaction(MySqlConnection mySQLConnection)
        {
            try
            {
                return mySQLConnection.BeginTransaction();
            }
            catch (MySqlException ex)
            {
                ErrorManager.Log(ex);
                return null;
            }
        }

        private static void CloseTransaction(MySqlTransaction mySQLTransaction, MySqlConnection mySqlConnection)
        {
            try
            {
                mySQLTransaction.Commit();
            }
            catch (MySqlException ex)
            {
                ErrorManager.Log(ex);
                Rollback(mySQLTransaction);
            }
            finally
            {
                Close(mySqlConnection);
            }
        }
    }
}
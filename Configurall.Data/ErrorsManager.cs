﻿using System;
using System.Runtime.CompilerServices;
using System.Text.Json;

namespace Configurall.Data
{
    public static class ErrorManager
    {
        public static void Log(Exception ex, [CallerMemberName] string callerMemberName="")
        {            
            Console.WriteLine($"{DateTime.Now} - {callerMemberName}. {ex.Message}. Inner exception: {ex.InnerException.Message}");
        }
    }
}

﻿using Configurall.Model.DBSchema;
using System.Collections.Generic;

namespace Configurall.Data
{
    /// <summary>
    /// Generates datatable entity models with Mdl sufix, and entity map fields with entity model name equals to table name.
    /// </summary>
    public static class ModelManager
    {
        public static void GenerateModels(string schema, string path)
        {
            SQLOperation.NonTransaction<bool>(c =>
            {
                var iSchema = c.Query<InformationSchemaMDL>(q =>
                    q.Select(InformationSchemaMDL._FD.TABLE_NAME)
                    .From(InformationSchemaMDL._FD.TableName)
                    .Where(InformationSchemaMDL._FD.TABLE_SCHEMA, schema)
                    .GroupBy(InformationSchemaMDL._FD.TABLE_NAME));

                var schemaTables = new Dictionary<string, IEnumerable<InformationSchemaMDL>>();
                foreach (var schemaTable in iSchema)
                    schemaTables.Add(schemaTable.TABLE_NAME,
                        c.Query<InformationSchemaMDL>(q =>
                            q.Select(InformationSchemaMDL._FD.All)
                            .From(InformationSchemaMDL._FD.TableName)
                            .Where(InformationSchemaMDL._FD.TABLE_NAME, schemaTable.TABLE_NAME)
                            .And(InformationSchemaMDL._FD.TABLE_SCHEMA, schema)));

                Model.Generator.Execute(schemaTables, path);
                return true;
            });
        }        
    }
}
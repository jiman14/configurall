﻿using Configurall.Model;
using Dapper;
using Dapper.Contrib.Extensions;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Configurall.Data
{
    public class SQLCommand
    {
        private MySqlConnection Connection;
        private MySqlTransaction Transaction;
        internal SQLCommand(MySqlConnection connection, MySqlTransaction transaction = null)
        {
            Connection = connection;
            Transaction = transaction;            
        }

        public T Get<T>(int id)
         where T : BaseModel
         => Connection.Get<T>(id, Transaction, commandTimeout: Connection.ConnectionTimeout);

        public IEnumerable<T> GetAll<T>()
           where T : BaseModel
           => Connection.GetAll<T>(Transaction, commandTimeout: Connection.ConnectionTimeout);

        public long Insert<T>(T obj)
            where T : BaseModel
            => Connection.Insert(obj, Transaction, commandTimeout: Connection.ConnectionTimeout);

        public long Insert<T>(IEnumerable<T> objs)
            where T : BaseModel
            => Connection.Insert(objs, Transaction, commandTimeout: Connection.ConnectionTimeout);

        public bool Update<T>(T obj)
            where T : BaseModel
            => Connection.Update(obj, Transaction, commandTimeout: Connection.ConnectionTimeout);

        public bool Update<T>(IEnumerable<T> objs)
        where T : BaseModel
            => Connection.Update(objs, Transaction, commandTimeout: Connection.ConnectionTimeout);

        public bool Delete<T>(T obj)
            where T : BaseModel
            => Connection.Delete(obj, Transaction, commandTimeout: Connection.ConnectionTimeout);

        public bool Delete<T>(IEnumerable<T> objs)
            where T : BaseModel
            => Connection.Delete(objs, Transaction, commandTimeout: Connection.ConnectionTimeout);

        public bool DeleteAll<T>()
            where T : BaseModel
            => Connection.DeleteAll<T>(Transaction, commandTimeout: Connection.ConnectionTimeout);

        public int DeleteCustom<T>(Action<CommandBuilder<T>> action)
            where T : BaseModel
        {
            var c = new CommandBuilder<T>();
            action.Invoke(c);
            return Connection.Execute(c.Query, param: c.Parameters, transaction: Transaction, commandTimeout: Connection.ConnectionTimeout);            
        }

        public IEnumerable<T> Query<T>(Action<QueryBuilder<T>> action)
            where T : BaseModel
        {
            var q = new QueryBuilder<T>();
            action.Invoke(q);
            return Connection.Query<T>(q.Query, param: q.Parameters, transaction: Transaction, commandTimeout: Connection.ConnectionTimeout);            
        }

        public T QueryOne<T>(Action<QueryBuilder<T>> action)
            where T : BaseModel
        {
            var q = new QueryBuilder<T>();
            action.Invoke(q);
            return Connection.Query<T>(q.Query, param: q.Parameters, transaction: Transaction, commandTimeout: Connection.ConnectionTimeout)?
                .FirstOrDefault() ?? default(T);
        }

        public int Count<T>(Action<QueryBuilder<T>> action)
            where T : BaseModel
        {
            var q = new QueryBuilder<T>();
            action.Invoke(q);
            return Connection.ExecuteScalar<int>(q.Query, param: q.Parameters, transaction: Transaction, commandTimeout: Connection.ConnectionTimeout);
        }
    }
}
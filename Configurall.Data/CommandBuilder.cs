﻿using Configurall.Model;
using System;

namespace Configurall.Data
{
    public class CommandBuilder<T>: BaseBuilder<T>
        where T: BaseModel
    {
        #region Commands
        public CommandBuilder<T> Delete(string tableName)
        {
            commandType = CommandType.delete;
            deleteQuery = $"delete from {tableName} ";

            return this;
        }
        #endregion

        #region Filter query
        public CommandBuilder<T> Where(string column, params string[] values) =>
            (CommandBuilder<T>)Where(true, column, Operator.Equals, values);
        public CommandBuilder<T> Where(string column, Operator op, params string[] values) =>
            (CommandBuilder<T>)Where(true, column, op, values);

        public CommandBuilder<T> And(string column, params string[] values) =>
            (CommandBuilder<T>)Where(true, column, Operator.Equals, values);

        public CommandBuilder<T> And(string column, Operator op, params string[] values) =>
            (CommandBuilder<T>)Where(true, column, op, values);

        public CommandBuilder<T> Or(string column, params string[] values) =>
            (CommandBuilder<T>)Where(false, column, Operator.Equals, values);

        public CommandBuilder<T> Or(string column, Operator op, params string[] values) =>
            (CommandBuilder<T>)Where(false, column, op, values);

        public CommandBuilder<T> Parenthesis(Action<CommandBuilder<T>> func)
        {
            OpenParenthesis();
            func.Invoke(this);
            CloseParenthesis();
            return this;
        }
        #endregion
    }

}

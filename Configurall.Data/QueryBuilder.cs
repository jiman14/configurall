﻿using Configurall.Model;
using System;
using System.Linq;

namespace Configurall.Data
{
    #region QueryBuilder<T> enums
    public enum BaseOperator
    {
        Distinct,
        Equals,
        Mayor,
        Minor
    }
    public enum JoinType
    {
        inner,
        left,
        right
    }
    public enum QueryOrderBy
    {
        asc,
        desc
    }
    #endregion

    public class QueryBuilder<T> : BaseBuilder<T>
        where T : BaseModel
    {
        #region Variables

        private bool IsLimitOne { get; set; }
        private bool IsSelectCount { get; set; }
        #endregion

        #region Select commands
        public QueryBuilder<T> Select(params string[] columnNames)
        {
            if (columnNames.Length == 0)
                throw new Exception("You must specify one column name at least");

            commandType = CommandType.select;
            IsSelectCount = false;
            selectFields = string.Join(",", columnNames);

            if (columnNames.FirstOrDefault().Contains("."))
                fromClause = columnNames.FirstOrDefault().Replace(
                    "." + columnNames.FirstOrDefault().Split(new char[] { '.' }).Last(), 
                    string.Empty);

            return this;
        }
        public QueryBuilder<T> SelectCount(string columnName = "*")
        {
            commandType = CommandType.select;
            IsSelectCount = true;
            selectFields = $"count({columnName})";

            if (columnName.Contains("."))
                fromClause = columnName.Replace("." + columnName.Split(new char[] { '.' }).Last(), 
                    string.Empty);

            return this;
        }
        #endregion

        #region From & Joins
        public QueryBuilder<T> From(string table)
        {
            fromClause = table;

            return this;
        }
        public QueryBuilder<T> Join(string leftColumn, BaseOperator op, string rightColumn) =>
            Join(JoinType.inner, leftColumn, op, rightColumn);
        public QueryBuilder<T> LeftJoin(string leftColumn, BaseOperator op, string rightColumn) =>
            Join(JoinType.left, leftColumn, op, rightColumn);
        public QueryBuilder<T> RightJoin(string leftColumn, BaseOperator op, string rightColumn) =>
            Join(JoinType.right, leftColumn, op, rightColumn);
        private QueryBuilder<T> Join(JoinType joinType, string leftColumn, BaseOperator op, string rightColumn)
        {
            var rightTable = rightColumn.Remove(rightColumn.IndexOf("."));
            var opStr = "";
            switch (op)
            {
                case BaseOperator.Distinct: opStr = "!="; break;
                case BaseOperator.Equals: opStr = "="; break;
                case BaseOperator.Mayor: opStr = ">"; break;
                case BaseOperator.Minor: opStr = "<"; break;
            };
            fromClause += $" {joinType.ToString()} join {rightTable} on ({leftColumn} {opStr} {rightColumn})";

            return this;
        }
        #endregion

        #region Filter query
        public QueryBuilder<T> Where(string column, params object[] values) =>
            (QueryBuilder<T>)Where(true, column, Operator.Equals, values);
        public QueryBuilder<T> Where(string column, Operator op, params object[] values) =>
            (QueryBuilder<T>)Where(true, column, op, values);

        public QueryBuilder<T> And(string column, params object[] values) =>
            (QueryBuilder<T>)Where(true, column, Operator.Equals, values);
        public QueryBuilder<T> And(string column, Operator op, params object[] values) =>
            (QueryBuilder<T>)Where(true, column, op, values);

        public QueryBuilder<T> Or(string column, params object[] values) =>
            (QueryBuilder<T>)Where(false, column, Operator.Equals, values);
        public QueryBuilder<T> Or(string column, Operator op, params object[] values) =>
            (QueryBuilder<T>)Where(false, column, op, values);

        public QueryBuilder<T> Parenthesis(Action<QueryBuilder<T>> func)
        {
            OpenParenthesis();
            func.Invoke(this);
            CloseParenthesis();
            return this;
        }
        #endregion

        #region Group, order & having

        public QueryBuilder<T> GroupBy(params string[] columnNames)
        {
            groupClause = $" group by {string.Join(",", columnNames)}";

            return this;
        }

        /// <summary>
        /// Fill orderColumns param with direction intercalated with (Ascending or Descending), for ex.: ID,Ascending,CREATION_DATE,Descending 
        /// Or fill orderColumns param with direction intercalated with (true or false), for ex.: ID,true,CREATION_DATE,false 
        /// Or fill orderColumns param with direction intercalated with (asc or desc), for ex.: ID,asc,CREATION_DATE,desc
        /// In no direction is specified, default order is asc.
        /// </summary>
        /// <param name="orderColumns"></param>
        /// <returns></returns>
        public QueryBuilder<T> OrderBy(params string[] orderColumns)
        {
            var sep = ",";
            var space = " ";
            var order = $" order by {string.Join(sep, orderColumns)}";
            orderClause = order
                .Replace($"{sep}{bool.TrueString}", $"{space}{QueryOrderBy.asc}")
                .Replace($"{sep}{bool.FalseString}", $"{space}{QueryOrderBy.desc}");
            orderClause = orderClause
                .Replace($"{sep}{QueryOrderBy.asc}", $"{space}{QueryOrderBy.asc}")
                .Replace($"{sep}{QueryOrderBy.desc}", $"{space}{QueryOrderBy.desc}");

            return this;
        }

        public QueryBuilder<T> Having(string expr, BaseOperator op, string value)
        {
            var having = "";
            switch (op)
            {
                case BaseOperator.Distinct: having = $"{expr} <> '{value}' "; break;
                case BaseOperator.Equals: having = $"{expr} = '{value}' "; break;
                case BaseOperator.Mayor: having = $"{expr} > '{value}' "; break;
                case BaseOperator.Minor: having = $"{expr} < '{value}' "; break;
            };
            havingClause = $" having ({having})";

            return this;
        }
        #endregion

        #region Limit query

        /// <summary>
        /// limit (0, 1)
        /// </summary>
        /// <returns></returns>
        public QueryBuilder<T> LimitOne() =>
            Limit(0, 1);
        /// <summary>
        /// limit (0, nRows)
        /// </summary>
        /// <param name="nRows"></param>
        /// <returns></returns>
        public QueryBuilder<T> Limit(int nRows) =>
            Limit(0, nRows);
        /// <summary>
        /// limit (fromRow, nRows)
        /// </summary>
        /// <param name="fromRow"></param>
        /// <param name="nRows"></param>
        /// <returns></returns>
        public QueryBuilder<T> Limit(int fromRow, int nRows)
        {
            IsLimitOne = fromRow == 0 && nRows == 1;

            LimitClause += $" limit {fromRow}, {nRows}";

            return this;
        }
        #endregion
    }
}
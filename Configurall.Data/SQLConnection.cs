﻿using MySql.Data.MySqlClient;

namespace Configurall.Data
{
    public class SQLConnection
    {
        protected static int CommandTimeout = 120;
        private static string ConnectionString { get; set; }

        public static void SetConnectionString(string connectionString)
        => ConnectionString = connectionString;        

        internal static MySqlConnection Open()
        {
            try
            {
                var mySQLConnection = new MySqlConnection(ConnectionString);
                mySQLConnection.Open();
                return mySQLConnection;
            }
            catch (MySqlException ex)
            {
                ErrorManager.Log(ex);
                return null;
            }
        }

        internal static void Close(MySqlConnection mySQLConnection)
        {
            if (mySQLConnection != null && mySQLConnection.State == System.Data.ConnectionState.Open)
                mySQLConnection.Close();
        }

    }
}

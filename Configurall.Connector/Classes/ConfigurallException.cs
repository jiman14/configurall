﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Configurall.Connector.Classes
{
    /// <summary>
    /// Configurall exception class
    /// </summary>
    public class ConfigurallNoConnectedException : Exception
    {
        /// <summary>
        /// Configurall source exception
        /// </summary>
        public override string Source { get => "Configurall connector exception"; set => base.Source = value; }

        /// <summary>
        /// Exception for API no connected
        /// </summary>
        public ConfigurallNoConnectedException()
        : base(message: "Configurall not connected, please initialize Instance class with API Url and API Key")
        {
        }
    }
}

﻿using System;
using System.Threading.Tasks;

namespace Configurall.Connector
{
    public interface IConfigurallBase
    {
        Task Initialize(Instance configurall);

    }
}

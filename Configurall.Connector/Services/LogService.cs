﻿using Microsoft.Extensions.Logging;
using System;
using System.Runtime.CompilerServices;

namespace Configurall.Connector.Services
{
    internal static class Log
    {
        static ILoggerFactory loggerFactory;

        static Log()
        {
            loggerFactory = LoggerFactory.Create(builder =>
            {
                builder
                    .AddFilter("Microsoft", LogLevel.Warning)
                    .AddFilter("System", LogLevel.Warning)
                    .AddFilter("GlobalUWS.UI", LogLevel.Debug)
                    .AddConsole();
            });
            //loggerFactory.AddFile("Logs/mylog-{Date}.txt");
        }
        internal static ILogger<T> Logger<T>()
        {
            return loggerFactory.CreateLogger<T>();
        }

        internal static void Info<T>(string message, [CallerMemberName] string callerMember = null,
            [CallerFilePath] string sourceFilePath = null,
            [CallerLineNumber] int sourceLineNumber = -1)
            => Logger<T>().LogInformation($"{formatCallers(sourceFilePath, callerMember, sourceLineNumber)}: {message}");


        internal static void Info<T>(string message, object arg, [CallerMemberName] string callerMember = null,
            [CallerFilePath] string sourceFilePath = null,
            [CallerLineNumber] int sourceLineNumber = -1)
            => Logger<T>().LogInformation($"{formatCallers(sourceFilePath, callerMember, sourceLineNumber)}: {message}", arg);

        internal static void Info<T>(Exception ex, string message, [CallerMemberName] string callerMember = null,
            [CallerFilePath] string sourceFilePath = null,
            [CallerLineNumber] int sourceLineNumber = -1)
            => Logger<T>().LogInformation(ex, $"{formatCallers(sourceFilePath, callerMember, sourceLineNumber)}: {message}");

        internal static void Info<T>(Exception ex, string message, object arg, [CallerMemberName] string callerMember = null,
            [CallerFilePath] string sourceFilePath = null,
            [CallerLineNumber] int sourceLineNumber = -1)
            => Logger<T>().LogInformation(ex, $"{formatCallers(sourceFilePath, callerMember, sourceLineNumber)}: {message}", arg);


        internal static void Debug<T>(string message, [CallerMemberName] string callerMember = null,
            [CallerFilePath] string sourceFilePath = null,
            [CallerLineNumber] int sourceLineNumber = -1)
            => Logger<T>().LogDebug($"{formatCallers(sourceFilePath, callerMember, sourceLineNumber)}: {message}");

        internal static void Debug<T>(string message, object arg, [CallerMemberName] string callerMember = null,
            [CallerFilePath] string sourceFilePath = null,
            [CallerLineNumber] int sourceLineNumber = -1)
            => Logger<T>().LogDebug($"{formatCallers(sourceFilePath, callerMember, sourceLineNumber)}: {message}", arg);

        internal static void Debug<T>(Exception ex, string message, [CallerMemberName] string callerMember = null,
            [CallerFilePath] string sourceFilePath = null,
            [CallerLineNumber] int sourceLineNumber = -1)
            => Logger<T>().LogDebug(ex, $"{formatCallers(sourceFilePath, callerMember, sourceLineNumber)}: {message}");

        internal static void Debug<T>(Exception ex, string message, object arg,
            [CallerMemberName] string callerMember = null,
            [CallerFilePath] string sourceFilePath = null,
            [CallerLineNumber] int sourceLineNumber = -1)
            => Logger<T>().LogDebug(ex, $"{formatCallers(sourceFilePath, callerMember, sourceLineNumber)}: {message}", arg);

        internal static void Error<T>(string message,
            [CallerMemberName] string callerMember = null,
            [CallerFilePath] string sourceFilePath = null,
            [CallerLineNumber] int sourceLineNumber = -1)
            => Logger<T>().LogError($"{formatCallers(sourceFilePath, callerMember, sourceLineNumber)}: {message}");

        internal static void Error<T>(string message, object arg,
            [CallerMemberName] string callerMember = null,
            [CallerFilePath] string sourceFilePath = null,
            [CallerLineNumber] int sourceLineNumber = -1)
            => Logger<T>().LogError($"{formatCallers(sourceFilePath, callerMember, sourceLineNumber)}: {message}", arg);

        internal static void Error<T>(Exception ex, string message, [CallerMemberName] string callerMember = null,
            [CallerFilePath] string sourceFilePath = null,
            [CallerLineNumber] int sourceLineNumber = -1)
            => Logger<T>().LogError(ex, $"{formatCallers(sourceFilePath, callerMember, sourceLineNumber)}: {message}");

        internal static void Error<T>(Exception ex,
            [CallerMemberName] string callerMember = null,
            [CallerFilePath] string sourceFilePath = null,
            [CallerLineNumber] int sourceLineNumber = -1)
            => Logger<T>().LogError(ex, $"{formatCallers(sourceFilePath, callerMember, sourceLineNumber)}: {ex.Message}");

        internal static void Error<T>(Exception ex, string message, object arg,
            [CallerMemberName] string callerMember = null,
            [CallerFilePath] string sourceFilePath = null,
            [CallerLineNumber] int sourceLineNumber = -1)
            => Logger<T>().LogError(ex, $"{formatCallers(sourceFilePath, callerMember, sourceLineNumber)}: {message}", arg);

        private static string formatCallers(string sourceFilePath, string callerMember, int sourceLineNumber = -1)
            => $@"{sourceFilePath.Substring((sourceFilePath.LastIndexOf("\\") > 0 ? sourceFilePath.LastIndexOf("\\") + 1 : 0))}.{callerMember} [{sourceLineNumber}]";
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using System.Text;
using Configurall.Connector.Services;
using Configurall.Shared.DTOs;

namespace Configurall.Connector
{
    internal class RestService : IDisposable
    {
        private const string JwtBearerScheme = "Bearer";
        HttpClient client;
        private string BaseURL;

        internal static string MediaType_APPJSON = "application/json";

        internal RestService(string baseURL, HttpMessageHandler httpMessageHandler=null)
        {
            client = httpMessageHandler== null? new HttpClient(): new HttpClient(httpMessageHandler);
            BaseURL = baseURL;
            client.Timeout = new TimeSpan(0, 0, 10);
        }

        internal void SetAccessToken(string token)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(JwtBearerScheme, token);
        }

        internal async Task<List<T>> Get<T>(string method)
        {
            try
            {
                Log.Info<RestService>(method);

                var res = await client.GetAsync(
                  BaseURL + method);

                if (res == null || !res.IsSuccessStatusCode)
                {
                    Log.Error<RestService>($"[Get: {method}] Status: {res.StatusCode}");
                    return default;
                }

                var response = await res.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<T>>(response);
            }
            catch (Exception ex)
            {
                Log.Error<RestService>(ex, message: method);
                return default;
            }
        }

        internal async Task<T> Post<T>(string method, object obj)
        {
            try
            {
                var jsonObj = JsonConvert.SerializeObject(obj);
                HttpContent content = new StringContent(jsonObj, Encoding.UTF8, RestService.MediaType_APPJSON);

                Log.Info<RestService>(method, content);

                var res = await client.PostAsync(
                  BaseURL + method, content);

                if (res == null || !res.IsSuccessStatusCode)
                    return default;

                string response = await res.Content.ReadAsStringAsync();

                if (string.IsNullOrEmpty(response))
                    return default(T);

                return JsonConvert.DeserializeObject<T>(response);
            }
            catch (Exception ex)
            {
                Log.Error<RestService>(ex, $"{BaseURL + method}", obj);
                return default;
            }
        }

        internal async Task<bool> Put(string method, object obj)
        {
            try
            {
                var jsonObj = JsonConvert.SerializeObject(obj);
                HttpContent content = new StringContent(jsonObj, Encoding.UTF8, RestService.MediaType_APPJSON);

                Log.Info<RestService>(method, content);

                var res = await client.PutAsync(BaseURL + method, content);

                return res != null && res.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                Log.Error<RestService>(ex, $"{BaseURL + method}", obj);
                return false;
            }
        }

        void IDisposable.Dispose()
            => client?.Dispose();        
    }

    internal class BORestService : RestService
    {
        internal BORestService(string baseURL) : base(baseURL) { }
    }
}

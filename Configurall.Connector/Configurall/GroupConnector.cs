﻿using Configurall.Shared.DTOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Configurall.Connector
{
    /// <summary>
    /// Configurall instance
    /// </summary>
    public class Group
    {
        const string GroupURL = "/" + nameof(Group);
        const string GetGroupMethodURL = GroupURL + "/" + nameof(Group.Get);
        const string GetGroupNamesURL = GroupURL + "/" + nameof(Group.GetNames);
        const string GetGroupRowsURL = GroupURL + "/" + nameof(Group.GetRows);
        const string SetGroupMethodURL = GroupURL + "/" + nameof(Group.Set);


        /// <summary>
        /// Get group names
        /// </summary>
        /// <param name="categoryName">Category name</param>
        /// <returns>Groups names</returns>
        public static async Task<IEnumerable<string>> GetNames(string categoryName)
        => await Instance.APIConnector.Post<IEnumerable<string>>(GetGroupNamesURL, new CategoryDTO { Name = categoryName });

        /// <summary>
        /// Get group rows
        /// </summary>
        /// <param name="categoryName">Category name</param>
        /// <param name="name">Group name</param>
        /// <returns>Row id list</returns>
        public static async Task<IEnumerable<string>> GetRows(string categoryName, string name)
        => await Instance.APIConnector.Post<IEnumerable<string>>(GetGroupRowsURL, new GroupDTO { CategoryName = categoryName, Name = name });

        /// <summary>
        /// Get groups names of category specified
        /// </summary>
        /// <param name="categoryName">Category name</param>
        /// <returns>Groups</returns>
        public static async Task<IEnumerable<GroupDTO>> Get(string categoryName)
        => await Instance.APIConnector.Post<IEnumerable<GroupDTO>>(GetGroupMethodURL, new CategoryDTO { Name = categoryName });


        /// <summary>
        /// Set group with inherits
        /// </summary>
        /// <param name="categoryName">Category name</param>
        /// <param name="name">Group name</param>
        /// <param name="inherits">Group inherits list</param>
        /// <returns>Result of the operation</returns>
        public static async Task<bool> Set(string categoryName, string name, params string[] inherits)
        => await Instance.APIConnector.Put(SetGroupMethodURL, new GroupDTO
        {
            CategoryName = categoryName,
            Name = name,
            Inherits = inherits?.ToList()
        });
    }
}

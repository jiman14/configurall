﻿using Configurall.Shared.DTOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Configurall.Connector
{
    /// <summary>
    /// Configurall instance
    /// </summary>
    public class Catalog
    {
        const string CatalogURL = "/" + nameof(Catalog);
        const string SetCatalogMethodURL = CatalogURL + "/" + nameof(Catalog.Set);
       
        /// <summary>
        /// Set catalog
        /// </summary>
        /// <param name="name">Catalog name</param>
        /// <param name="content">Catalog content as array</param>
        /// <returns></returns>
        public static async Task<bool> Set(string name, IEnumerable<string> content)
            => await Set(name, content, new string[0]);

        /// <summary>
        /// Set catalog from enum
        /// </summary>
        /// <typeparam name="T">Enum type</typeparam>
        /// <returns></returns>
        public static async Task<bool> SetFromEnum<T>() where T: Enum
        => await Set(typeof(T).FullName, Enum.GetNames(typeof(T)), new string[0]);

        /// <summary>
        /// Set catalog base
        /// </summary>
        /// <param name="name">Catalog name</param>
        /// <param name="content">Catalog content object</param>
        /// <param name="schema">Catalog content empty object for schema</param>
        /// <returns></returns>
        public static async Task<bool> Set(string name, object content, object schema)
        => await Instance.APIConnector.Put(SetCatalogMethodURL, new CatalogDTO
        {
            Name = name,
            Content = JsonConvert.SerializeObject(content),
            Schema = JsonConvert.SerializeObject(schema)
        });
    }
}

﻿using Configurall.Shared.DTOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Configurall.Connector
{
    /// <summary>
    /// Configurall instance
    /// </summary>
    public class Config
    {
        #region Vars & initialization
        const string ConfigurallURL = "/Configurall";
        const string GetMethodURL = ConfigurallURL + "/GetConfiguration";
        const string SetMethodURL = ConfigurallURL + "/SetConfiguration";
        const string SetGroupCategoryMethodURL = ConfigurallURL + "/SetGroupCategory";
        const string SetGroupMethodURL = ConfigurallURL + "/SetGroup";
        const string SetDomainMethodURL = ConfigurallURL + "/SetDomain";

        const string GetCategoriesURL = ConfigurallURL + "/GetCategories";
        const string GetGroupsURL = ConfigurallURL + "/GetGroups";
        const string GetConfigUniverseURL = ConfigurallURL + "/GetConfigUniverse";
        const string GetParentGroupsRowIdsURL = ConfigurallURL + "/GetParentGroupsRowIds";        

        const string DeleteConfigurationURL = ConfigurallURL + "/DeleteConfiguration";

        #endregion

        #region Getters

        public static async Task<IEnumerable<string>> GetCategories()
        => await Instance.APIConnector.Get<string>(GetCategoriesURL);

        public static async Task<IEnumerable<string>> GetParentGroupsRowIds(string categoryName, string groupName)
        => await Instance.APIConnector.Post<IEnumerable<string>>(GetParentGroupsRowIdsURL, new GroupCategoryDTO
        {
            CategoryName = categoryName,
            GroupName = groupName
        });        

        public static async Task<IEnumerable<ConfigResDTO>> GetConfigUniverse(string categoryName, string groupName)
        => await Instance.APIConnector.Post<IEnumerable<ConfigResDTO>>(GetConfigUniverseURL, new GroupCategoryDTO { CategoryName = categoryName, GroupName = groupName });

        public static async Task<T> GetConfiguration<T>(string categoryName, string groupName, string rowId)
            where T : Configurable
        {
#if !DEBUG
            var dictId = $"{groupName}_{rowid}";

            if (ConfigRepo.ContainsKey(dictId))
                return (T)ConfigRepo[dictId];
#endif

            var configObj = new ConfigDTO
            {
                CategoryName = categoryName,
                GroupName = groupName,
                RowId = rowId,
                EntityName = typeof(T).FullName
            };

            var obj = await Instance.APIConnector.Post<T>(GetMethodURL, configObj);

#if !DEBUG
            ConfigRepo.TryAdd(dictId, obj);
#endif

            return obj;
        }

        public static async Task<bool> NewGroupEntityRowId(string categoryName, string groupName, string entityTypeFullName, string newRowId)
        {
            await Instance.APIConnector.Post<Configurable>(GetMethodURL, new ConfigDTO
            {
                CategoryName = categoryName,
                GroupName = groupName,
                RowId = newRowId,
                EntityName = entityTypeFullName
            });
            return true;
        }

        #endregion

        #region Setters

        /// <summary>
        /// Set configuration for entity T
        /// </summary>
        /// <typeparam name="T">Entity Type</typeparam>
        /// <param name="categoryName">Group category</param>
        /// <param name="groupName">Group name</param>
        /// <param name="rowId">Row Id</param>
        /// <param name="content">Entity object</param>
        /// <returns></returns>
        public static async Task<bool> SetConfiguration<T>(string categoryName, string groupName, string rowId, T content) where T: Configurable
        => await Instance.APIConnector.Put(SetMethodURL, new SetConfigDTO { 
            CategoryName = categoryName, 
            GroupName = groupName, 
            EntityName = typeof(T).FullName,
            RowId = rowId,
            Content = JsonConvert.SerializeObject(content)
        });

        /// <summary>
        /// Set configuration for entity T
        /// </summary>
        /// <param name="categoryName">Group category name</param>
        /// <param name="groupName">Group name</param>
        /// <param name="entityTypeFullName">Full name property of entity type (ej. typeof(...).FullName)</param>
        /// <param name="rowId">Row Id</param>
        /// <param name="entityJson">Entity content serialized to Json</param>
        /// <returns></returns>
        public static async Task<bool> SetConfiguration(string categoryName, string groupName, string entityTypeFullName, string rowId, string entityJson)
        => await Instance.APIConnector.Put(SetMethodURL, new SetConfigDTO
        {
            CategoryName = categoryName,
            GroupName = groupName,
            EntityName = entityTypeFullName,
            RowId = rowId,
            Content = entityJson
        });

        /// <summary>
        /// Set Domain from enum
        /// </summary>
        /// <param name="enumType"></param>
        /// <returns></returns>
        public static async Task<bool> SetDomainFromEnum(Type enumType)
        => await SetDomain(enumType.Name, false, (string[])Enum.GetValues(enumType));
        
        /// <summary>
        /// Set group catogory
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static async Task<bool> SetGroupCategory(string name)
        => await Instance.APIConnector.Put(SetGroupCategoryMethodURL, new CategoryDTO { Name= name });       

        public static async Task<bool> SetDomain(string name, bool mandatory, IEnumerable<string> values)
        => await Instance.APIConnector.Put(SetDomainMethodURL, new DomainDTO 
        { 
           Name = name, 
           Mandatory = mandatory,
           Values=  values
        });

        #endregion

        /// <summary>
        /// Delete configuration of entity E for rowId
        /// </summary>
        /// <typeparam name="E">Entity type</typeparam>
        /// <param name="groupCategory">Group category name</param>
        /// <param name="groupName">Group name</param>
        /// <param name="rowId">Row Id</param>
        /// <returns></returns>
        public static async Task<bool> DeleteConfiguration<E>(string groupCategory, string groupName, string rowId) where E : Configurable
        => await DeleteConfiguration(groupCategory, groupName, rowId, typeof(E).FullName);

        /// <summary>
        /// Delete configuration
        /// </summary>
        /// <param name="groupCategory">Group category name</param>
        /// <param name="groupName">Group name</param>
        /// <param name="rowId">Row Id</param>
        /// <param name="entityName">Entity name</param>
        /// <returns></returns>
        public static async Task<bool> DeleteConfiguration(string groupCategory, string groupName, string rowId, string entityName)
        => await Instance.APIConnector.Put(DeleteConfigurationURL, new ConfigDTO {
            GroupCategory = groupCategory,
            GroupName = groupName,
            RowId = rowId,
            EntityName = entityName
        });

    }


}

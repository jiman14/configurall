﻿using Configurall.Shared.DTOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Configurall.Connector
{
    /// <summary>
    /// Configurall instance
    /// </summary>
    public class Entity
    {
        const string EntityURL = "/" + nameof(Entity);

        const string SetEntityMethodURL = EntityURL + "/" + nameof(Entity.Set);
        const string GetEntitiesURL = EntityURL + "/" + nameof(Entity.GetAll);

        /// <summary>
        /// Get all entities
        /// </summary>
        /// <returns></returns>
        public static async Task<IEnumerable<EntityDTO>> GetAll()
       => await Instance.APIConnector.Get<EntityDTO>(GetEntitiesURL);

        /// <summary>
        /// Set Entity of T type, thats inherits from I type nad group name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="I"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static async Task<bool> Set<T, I>(T entity) where T : Configurable where I : Configurable
        => await Set<T>(entity, new string[] { typeof(I).FullName });

        /// <summary>
        /// Set entity T, and inheritance dictionary
        /// </summary>
        /// <typeparam name="T">entity type</typeparam>
        /// <param name="entity">entity object</param>
        /// <param name="inherits">Entity inheritance (full type name and group name)</param>
        /// <returns></returns>
        public static async Task<bool> Set<T>(T entity, params string[] inherits) where T : Configurable
        => await Set(typeof(T).FullName, JsonConvert.SerializeObject(entity), inherits);

        /// <summary>
        /// Set entity
        /// </summary>
        /// <param name="entityTypeFullName">Entity type full name</param>
        /// <param name="schemaJson">Entity object schema</param>
        /// <returns></returns>
        public static async Task<bool> Set(string entityTypeFullName, string schemaJson)
            => await Set(entityTypeFullName, schemaJson, null);

        /// <summary>
        /// Set entity 
        /// </summary>
        /// <param name="entityTypeFullName">Full name property of entity type (ej. typeof(...).FullName)</param>
        /// <param name="schemaJson">Entity serialized to Json</param>
        /// <param name="inherits">Entity inheritance (full type name and group name)</param>
        /// <returns></returns>
        public static async Task<bool> Set(string entityTypeFullName, string schemaJson, params string[] inherits)
        => await Instance.APIConnector.Put(SetEntityMethodURL, new EntityDTO
        {
            Name = entityTypeFullName,
            Inherits = inherits?.ToList(),
            Schema = schemaJson
        });

        /// <summary>
        /// Set Entity T with Catalog
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <param name="entity">Entity object</param>
        /// <param name="entityProperty">Entity property name (use nameof)</param>
        /// <param name="catalogName">Entity catalog name</param>
        /// <param name="catalogContent">Entity catalog content as string array</param>
        /// <returns></returns>
        public static async Task<bool> SetWithCatalog<T>(T entity, string entityProperty, string catalogName, IEnumerable<string> catalogContent) where T : Configurable
            => await SetWithCatalog(entity, entityProperty, catalogName, catalogContent, null);

        /// <summary>
        /// Set entity with catalog
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <param name="entity">Entity object</param>
        /// <param name="entityProperty">Entity property name (use nameof)</param>
        /// <param name="catalogName">Entity catalog name</param>
        /// <param name="catalogContent">Entity catalog content as string array</param>
        /// <param name="inherits">Entity inheritance (type full name and group name)</param>
        /// <returns></returns>
        public static async Task<bool> SetWithCatalog<T>(T entity, string entityProperty, string catalogName, IEnumerable<string> catalogContent,
            params string[] inherits) where T : Configurable
        {
            var res = await Entity.Set<T>(entity, inherits);
            if (!res) return false;
            res = await Catalog.Set(catalogName, catalogContent);
            if (!res) return false;
            return await EntityCatalog.Set<T>(entityProperty, catalogName);
        }

        /// <summary>
        /// Set entity (with parent entity), catalog and entity-catalog relation
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <typeparam name="E">Enum type</typeparam>
        /// <typeparam name="I">Entity enherits</typeparam>
        /// <param name="entity">Entity object</param>
        /// <param name="entityProperty">Entity property linked to catalog</param>
        /// <returns></returns>
        public static async Task<bool> SetWithEnumCatalog<T, E, I>(T entity, string entityProperty) where T : Configurable where E : Enum where I : Configurable
            => await SetWithEnumCatalog<T, E>(entity, entityProperty, new string[] { typeof(I).FullName });

        /// <summary>
        /// Set entity, catalog and entity-catalog relation
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <typeparam name="E">Enum type</typeparam>
        /// <param name="entity">Entity object</param>
        /// <param name="entityProperty">Entity property linked to catalog</param>
        /// <returns></returns>
        public static async Task<bool> SetWithEnumCatalog<T, E>(T entity, string entityProperty) where T : Configurable where E : Enum
            => await SetWithEnumCatalog<T, E>(entity, entityProperty, null);

        /// <summary>
        /// Set entity, catalog and entity-catalog relation
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <typeparam name="E">Enum type</typeparam>
        /// <param name="entity">Entity object</param>
        /// <param name="entityProperty">Entity property linked to catalog</param>
        /// <param name="inherits">Entity inheritance Type full name and group name</param>
        /// <returns></returns>
        public static async Task<bool> SetWithEnumCatalog<T, E>(T entity, string entityProperty, params string[] inherits) where T : Configurable where E : Enum
        {
            var res = await Entity.Set<T>(entity, inherits);
            if (!res) return false;
            res = await Catalog.SetFromEnum<E>();
            if (!res) return false;
            return await EntityCatalog.Set<T>(entityProperty, typeof(E).FullName);
        }

    }

}

﻿using Configurall.Connector.Classes;
using Configurall.Connector.Services;
using Configurall.Shared.DTOs;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Configurall.Connector
{
    /// <summary>
    /// Connector base instance
    /// </summary>
    public class Instance
    {
        private static RestService apiConnector;
        internal static RestService APIConnector
        {
            get {
                if (apiConnector == null)
                    throw new ConfigurallNoConnectedException();

                return apiConnector;
            }
            set { apiConnector = value; }
        }
        ConcurrentDictionary<string, object> ConfigRepo = new ConcurrentDictionary<string, object>();
        private string APIKey { get; set; }

        const string ConfigurallURL = "/Configurall";
        const string LoginMethodURL = ConfigurallURL + "/Login";

        /// <summary>
        /// Configure instance
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="configurallBaseURL"></param>
        /// <param name="httpMessageHandler"></param>
        public Instance(string apiKey, string configurallBaseURL, HttpMessageHandler httpMessageHandler = null)
        {
            APIKey = apiKey;
            APIConnector = new RestService(configurallBaseURL, httpMessageHandler);
        }
        /// <summary>
        /// API login and initialize configurable objects
        /// </summary>
        /// <param name="configurallBaseObjects">Configurable objects array</param>
        /// <returns></returns>
        public async Task<bool> Initialization(IEnumerable<IConfigurallBase> configurallBaseObjects)
        {
            var loginRes = await APIConnector.Post<LoginResDTO>(LoginMethodURL, new { APIKey });
            if (loginRes == null)
            {
                Log.Error<Instance>("Error on login");
                return false;
            }

            APIConnector.SetAccessToken(loginRes.APIKey);
#if DEBUG
            try
            {
                foreach (var configurableObj in configurallBaseObjects)
                    await configurableObj.Initialize(this);
            }
            catch (Exception exc)
            {
                Log.Error<Instance>(exc);
                return false;
            }
#endif
            return true;
        }

        /// <summary>
        /// Clean configuration repository cache
        /// </summary>
        public void CleanRepo() => ConfigRepo.Clear();
    }
}

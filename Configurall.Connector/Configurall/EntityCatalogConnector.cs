﻿using Configurall.Shared.DTOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Configurall.Connector
{
    /// <summary>
    /// Configurall instance
    /// </summary>
    public class EntityCatalog
    {
        const string EntityCatalogURL = "/" + nameof(EntityCatalog);
        const string SetEntityCatalogMethodURL = EntityCatalogURL + "/" + nameof(EntityCatalog.Set);
        const string GetEntityCatalogMethodURL = EntityCatalogURL + "/" + nameof(EntityCatalog.Get);
        const string DeleteEntityCatalogMethodURL = EntityCatalogURL + "/" + nameof(EntityCatalog.Delete);

        /// <summary>
        /// Get entity catalog
        /// </summary>
        /// <returns></returns>
        public static async Task<IEnumerable<EntityCatalogDTO>> Get()
        => await Instance.APIConnector.Get<EntityCatalogDTO>(GetEntityCatalogMethodURL);

        /// <summary>
        /// Set entityCatalog
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <typeparam name="C">Catalog type</typeparam>
        /// <param name="entityProperty"></param>
        /// <returns></returns>
        public static async Task<bool> Set<T, C>(string entityProperty) where T : Configurable
        => await Set<T>(entityProperty, typeof(C).FullName, null);

        /// <summary>
        /// Set entity (T) catalog 
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <param name="entityProperty">Entity property name</param>
        /// <param name="catalogName">Catalog name</param>
        /// <returns></returns>
        public static async Task<bool> Set<T>(string entityProperty, string catalogName) where T: Configurable
        => await Set<T>(entityProperty, catalogName, null);

        /// <summary>
        /// Set entity catalog
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <param name="entityProperty">Entity property name (use nameof)</param>
        /// <param name="catalog">entity catalog name</param>
        /// <param name="jsonPath">json path for catalog</param>
        /// <returns></returns>
        public static async Task<bool> Set<T>(string entityProperty, string catalog, string jsonPath)
        => await Instance.APIConnector.Put(SetEntityCatalogMethodURL, new EntityCatalogDTO 
        { 
            Entity = typeof(T).FullName, 
            EntityProperty= entityProperty, 
            Catalog = catalog, 
            JsonPath = jsonPath 
        });

        /// <summary>
        /// Delete entity catalog
        /// </summary>
        /// <param name="entityCatalogDTO"></param>
        /// <returns></returns>
        public static async Task<bool> Delete(DeleteEntityCatalogDTO entityCatalogDTO)
        => await Instance.APIConnector.Put(DeleteEntityCatalogMethodURL, entityCatalogDTO);
    }
}

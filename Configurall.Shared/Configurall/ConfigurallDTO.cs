﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Configurall.Shared.DTOs
{
    public class GroupCategoryDTO
    {
        [Required]
        public string CategoryName { get; set; }
        [Required]
        public string GroupName { get; set; }
    }

    public class SetConfigDTO : ConfigDTO
    {
        [Required]
        public string Content { get; set; }
    }

    public class ConfigDTO
    {
        [Required]
        public string CategoryName { get; set; }
        [Required]
        public string GroupName { get; set; }
        [Required]
        public string RowId { get; set; }
        [Required]
        public string EntityName { get; set; }
    }
    public class ConfigResDTO
    {
        public Guid Id { get; set; }
        public string CategoryName { get; set; }
        public string GroupName { get; set; }
        public string RowId { get; set; }
        public string EntityName { get; set; }
        public IEnumerable<string> EntityInherits { get; set; } = new string[0];
        public string EntitySchema { get; set; }
        public string Content { get; set; }
        public Dictionary<string, string> RowDomains { get; set; }
        public IEnumerable<ECatalogDTO> EntityCatalogs { get; set; } = new ECatalogDTO[0];
    }
    public class ECatalogDTO
    {
        public string EntityPropertyName { get; set; }
        public IEnumerable<string> Catalog { get; set; }
    }

    public class DomainDTO
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public bool Mandatory { get; set; }
        [Required]
        public IEnumerable<string> Values { get; set; }
    }

    public class DeleteDomainDTO
    {
        [Required]
        public string Name { get; set; }
    }

    public class ActiveDomainDTO
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Value { get; set; }
    }
    public class CategoryDTO
    {
        [Required]
        public string Name { get; set; }
    }

    public class GroupDTO
    {
        [Required]
        public string CategoryName { get; set; }
        [Required]
        public string Name { get; set; }
        public IEnumerable<string> Inherits { get; set; }
    }

    public class DeleteGroupDTO
    {
        [Required]
        public string CategoryName { get; set; }
        [Required]
        public string Name { get; set; }
    }

        public class EntityDTO
    {
        [Required]
        public string Name { get; set; }
        public IEnumerable<string> Inherits { get; set; }
        [Required]
        public string Schema { get; set; }
    }

    public class DeleteEntityDTO
    {
        [Required]
        public string Name { get; set; }
    }

    public class CatalogDTO
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public string Schema { get; set; }
    }
    public class DeleteCatalogDTO
    {
        [Required]
        public string Name { get; set; }
    }
    public class EntityCatalogDTO
    {
        [Required]
        public string EntityName { get; set; }
        [Required]
        public string EntityPropertyName { get; set; }
        [Required]
        public string CatalogName { get; set; }
        public string CatalogJsonPath { get; set; }
    }
    public class DeleteEntityCatalogDTO
    {
        [Required]
        public string EntityName { get; set; }
        [Required]
        public string EntityPropertyName { get; set; }
        [Required]
        public string CatalogName { get; set; }
    }

}

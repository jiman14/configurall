﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Configurall.Shared.DTOs
{
    public class LoginDTO
    {
        [Required]
        public string APIKey { get; set; }
    }
    public class LoginResDTO
    {
        public string APIKey { get; set; }
    }
   
}

﻿using System.ComponentModel.DataAnnotations;

namespace Configurall.Shared.DTOs
{
    public class ConfigurallDTO : BaseDTO
    {
        public int Id { get; set; }
        public string Config { get; set; }        
    }
}

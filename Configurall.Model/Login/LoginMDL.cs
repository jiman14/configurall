﻿using Titania.DBCore;

namespace Configurall.Model
{
    public enum LoginStatus
    {
        Active = 0,
        Disabled = 1,
        NotFound = 2
    }

    public class LoginMDL: BaseModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public LoginStatus Status { get; set; }
        public long LastActivityDate { get; set; }
        public long CreationDate { get; set; }
    }
}

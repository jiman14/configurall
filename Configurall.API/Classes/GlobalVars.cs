﻿using Microsoft.Extensions.Configuration;

namespace Configurall.API.Classes
{
    public class GlobalVars
    {
        public static string APIKey { get; set; }
        public static string BasicAuthSecret { get; set; }
        public static int BasicAuthTokenExpirationInDays { get; set; }
        public static string ConnectionString { get; set; }

        public GlobalVars(IConfiguration configuration)
        {
            var configurallSettings = configuration.GetSection(nameof(Configurall));
            APIKey = configurallSettings[nameof(APIKey)];
            BasicAuthSecret = configurallSettings[nameof(BasicAuthSecret)];
            BasicAuthTokenExpirationInDays = configurallSettings.GetValue<int>(nameof(BasicAuthTokenExpirationInDays));
            ConnectionString = configurallSettings[nameof(ConnectionString)];
        }
    }
}

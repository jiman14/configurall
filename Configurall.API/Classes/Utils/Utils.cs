﻿using Configurall.API.ConfigItems;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Configurall.API.Classes.Utils
{
    public static class DictionaryUtils
    {
        public static T Get<T>(this ConcurrentDictionary<string, T> dict, string key)
            where T : BaseConfig
            => dict.GetBase(key);

        public static T Get<T>(this ConcurrentDictionary<Guid, T> dict, Guid key)
            where T : BaseConfig
        => dict.GetBase(key);

        private static T GetBase<D, T>(this ConcurrentDictionary<D, T> dict, D key)
            where T : BaseConfig
        {
                if (dict.ContainsKey(key))
                    return dict[key];

                var obj = (T)Activator.CreateInstance(typeof(T), new object[] { key });

            bool res = false;
            while (!res)
                res = dict.TryAdd(key, obj);

            return obj;
        }

        public static Dictionary<string, string> Get(this ConcurrentDictionary<string, Dictionary<string, string>> dict, string key)
            => dict.GetDictionaryBase(key);

        private static Dictionary<string, string> GetDictionaryBase<D>(this ConcurrentDictionary<D, Dictionary<string, string>> dict, D key)
        {
            if (dict.ContainsKey(key))
                return dict[key];

            var objDict = new Dictionary<string, string> { };
            bool res = false;
            while (!res)
                res = dict.TryAdd(key, objDict);

            return objDict;
        }

        public static bool TryAdd(this Dictionary<string, string> enumerable, string key, string value)
        {
            if (enumerable.ContainsKey(value))
                return true;

            enumerable.Add(key, value);
            return true;
        }

    }
}

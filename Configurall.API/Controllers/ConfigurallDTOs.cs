﻿using Configurall.API.Classes;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Configurall.API.Controllers
{
    public class LoginDTO
    {
        [Required]
        public string APIKey { get; set; }
    }

    public class CategoryGroupDTO
    {
        [Required]
        public string CategoryName { get; set; }
        [Required]
        public string GroupName { get; set; }
    }

    public class SetConfigDTO: ConfigDTO
    {
        [Required]
        public string Content { get; set; }
    }

    public class ConfigDTO
    {
        [Required]
        public string GroupCategory { get; set; }
        [Required]
        public string GroupName { get; set; }
        [Required]
        public string RowId { get; set; }
        [Required]
        public string EntityName { get; set; }
    }
    public class ConfigResDTO
    {
        public string RowId { get; set; }
        public string EntityName { get; set; }
        public List<string> EntityInherits { get; set; } = new List<string>();
        public string EntitySchema { get; set; }
        public string EntityContent { get; set; }
        public IEnumerable<string> RowDomains { get; set; }
        public IEnumerable<ECatalogDTO> EntityCatalogs { get; set; } = new List<ECatalogDTO>();
    }
    public class ECatalogDTO
    {
        public string EntityProperty { get; set; }
        public IEnumerable<string> Catalog { get; set; }
    }

    public class DomainDTO
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Value { get; set; }
    }
    public class GroupCategoryDTO
    {
        [Required]
        public string Name { get; set; }
    }

    public class GroupDTO
    {
        [Required]
        public string CategoryName { get; set; }
        [Required]
        public string Name { get; set; }
        public List<string> Inherits { get; set; }
    }

    public class EntityDTO
    {
        [Required]
        public string Name { get; set; }
        public List<string> Inherits { get; set; }
        [Required]
        public string Schema { get; set; }
    }

    public class CatalogDTO
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public string Schema { get; set; }
    }
    public class EntityCatalogDTO
    {
        [Required]
        public string Entity { get; set; }
        [Required]
        public string EntityProperty { get; set; }
        [Required]
        public string Catalog { get; set; }
        public string JsonPath { get; set; }
    }
}

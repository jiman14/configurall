﻿using Configurall.API.ConfigItems;
using Configurall.API.Managers;
using Configurall.Services;
using Configurall.Shared.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Configurall.API.Controllers
{
    [Authorizatation]
    [ApiController]
    [Route(nameof(Catalog))]
    public class CatalogController : ConfigurallBaseController
    {
        public CatalogController(ILogger<ConfigurallBaseController> logger) : base(logger) { }

        [HttpPut(nameof(Set))]
        public async Task<bool> Set(CatalogDTO catalogDTO)
        => await ExecuteSave((token) => { return ConfigurationMgr.Configurall.SetCatalog(catalogDTO); });

    }


}

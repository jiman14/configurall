﻿using Configurall.API.ConfigItems;
using Configurall.API.Managers;
using Configurall.Services;
using Configurall.Shared.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Configurall.API.Controllers
{
    [Authorizatation]
    [ApiController]
    [Route(nameof(Entity))]
    public class EntityController : ConfigurallBaseController
    {
        public EntityController(ILogger<ConfigurallBaseController> logger) : base(logger) { }


        [HttpGet(nameof(GetAll))]
        public async Task<IEnumerable<EntityDTO>> GetAll()
        => await Execute((token) => { return ConfigurationMgr.Configurall.GetEntities(); });

        [HttpPut(nameof(Set))]
        public async Task<bool> Set(EntityDTO entityDTO)
        => await ExecuteSave((token) => { return ConfigurationMgr.Configurall.SetEntity(entityDTO); });

    }
}

﻿using Configurall.API.Managers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Configurall.API.Controllers
{
    public class ConfigurallBaseController: ControllerBase
    {
        private readonly ILogger<ConfigurallBaseController> _logger;

        public ConfigurallBaseController(ILogger<ConfigurallBaseController> logger) => _logger = logger;

        public async Task<T> Execute<T>(Func<string, T> func)
            => await ExecuteSave(func, false);

        public async Task<T> ExecuteSave<T>(Func<string, T> func, bool save = true)
        {
            try
            {
                var res = await ConfigurationMgr.LoadConfigurall();
                if (!res)
                {
                    _logger.LogError("Error loading data");
                    return default(T);
                }
                var token = HttpContext.Request.Headers["Authorization"].ToString();
                var resContent = func(token);
                if (save && !ConfigurationMgr.SaveConfigurall())
                {
                    _logger.LogError("Error saving data");
                    return default(T);
                }
                return resContent;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error");
                return default(T);
            }
        }
    }
}

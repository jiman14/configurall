﻿using Configurall.API.Managers;
using Configurall.Services;
using Configurall.Shared.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Configurall.API.Controllers
{
    [ApiController]
    [Route(nameof(Configurall))]
    public class LoginController : ControllerBase
    {
        private readonly ILogger _logger = LogMgr.Create<LoginController>();

        [HttpPost(nameof(Login))]
        public ActionResult<LoginDTO> Login(LoginDTO loginDTO)
        {
            if (loginDTO == null || string.IsNullOrEmpty(loginDTO.APIKey))
                return BadRequest();

            var token = SrvAccess.Instance.Login(loginDTO.APIKey);

            if (token == null)
                return NotFound();

            ConfigurationMgr.Init();

            return Ok(new LoginDTO { APIKey = token });            
        }
    }

}



﻿using Configurall.API.ConfigItems;
using Configurall.API.Managers;
using Configurall.Services;
using Configurall.Shared.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Configurall.API.Controllers
{
    [Authorizatation]
    [ApiController]
    [Route(nameof(Group))]
    public class GroupController : ConfigurallBaseController
    {
        public GroupController(ILogger<ConfigurallBaseController> logger) : base(logger) { }

        [HttpPost(nameof(Get))]
        public async Task<IEnumerable<GroupDTO>> Get(CategoryDTO categoryDTO)
        => await Execute((token) => { return ConfigurationMgr.Configurall.GetGroups(categoryDTO); });

        [HttpPost(nameof(GetNames))]
        public Task<IEnumerable<string>> GetNames(CategoryDTO categoryDTO)
        => Execute((token) => { return ConfigurationMgr.Configurall.GetGroupsNames(categoryDTO); });

        [HttpPost(nameof(GetRows))]
        public Task<IEnumerable<string>> GetRows(GroupDTO groupDTO)
        => Execute((token) => { return ConfigurationMgr.Configurall.GetGroupsRows(groupDTO); });

        [HttpPut(nameof(Set))]
        public async Task<bool> Set(GroupDTO groupDTO)
        => await ExecuteSave((token) => { return ConfigurationMgr.Configurall.SetGroup(groupDTO); });
    }
}

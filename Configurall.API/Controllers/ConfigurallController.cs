﻿using Configurall.API.Managers;
using Configurall.Services;
using Configurall.Shared.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Configurall.API.Controllers
{
    [Authorizatation]
    [ApiController]
    [Route(nameof(Configurall))]
    public class ConfigurallController : ConfigurallBaseController
    {
        public ConfigurallController(ILogger<ConfigurallBaseController> logger) : base(logger) { }

        [HttpGet(nameof(Reload))]
        public async void Reload() => await ConfigurationMgr.Reload();

        #region Getters

        [HttpPost(nameof(GetConfiguration))]
        public async Task<string> GetConfiguration(ConfigDTO configDTO)
        => await ExecuteSave((token) => { return ConfigurationMgr.Configurall.GetConfig(token, configDTO).Content; });

        [HttpGet(nameof(GetCategories))]
        public async Task<IEnumerable<string>> GetCategories()
        => await Execute((token) => { return ConfigurationMgr.Configurall.GetCategories(); });
        

        [HttpPost(nameof(GetConfigUniverse))]
        public Task<IEnumerable<ConfigResDTO>> GetConfigUniverse(GroupCategoryDTO categoryGroupDTO)
        => Execute((token) => { return ConfigurationMgr.Configurall.GetConfigAndEntities(categoryGroupDTO); });

        [HttpPost(nameof(GetParentGroupsRowIds))]
        public Task<IEnumerable<string>> GetParentGroupsRowIds(GroupCategoryDTO categoryGroupDTO)
        => Execute((token) => { return ConfigurationMgr.Configurall.GetParentGroupsRowIds(categoryGroupDTO); });

        #endregion

        #region Setters

        [HttpPut(nameof(SetDomain))]
        public async Task<bool> SetDomain(DomainDTO domainDTO)
        => await ExecuteSave((token) => { ConfigurationMgr.Configurall.SetDomain(domainDTO); return true; });

        [HttpPut(nameof(SetActiveDomain))]
        public async Task<bool> SetActiveDomain(ActiveDomainDTO domainDTO)
        => await ExecuteSave((token) => { return ConfigurationMgr.Configurall.SetActiveDomain(token, domainDTO); });

        [HttpPut(nameof(SetGroupCategory))]
        public async Task<bool> SetGroupCategory(CategoryDTO categoryDTO)
        => await ExecuteSave((token) => { return ConfigurationMgr.Configurall.SetGroupCategory(categoryDTO); });


        [HttpPut(nameof(SetConfiguration))]
        public async Task<bool> SetConfiguration(SetConfigDTO configDTO)
        => await ExecuteSave((token) => { return ConfigurationMgr.Configurall.SetConfig(token, configDTO); });

        #endregion

        #region Deletion

        [HttpPut(nameof(DeleteConfiguration))]
        public async Task<bool> DeleteConfiguration(ConfigDTO configDTO)
        => await ExecuteSave((token) => { return ConfigurationMgr.Configurall.DeleteConfig(token, configDTO); });
        #endregion


    }


}

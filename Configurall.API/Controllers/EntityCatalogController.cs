﻿using Configurall.API.ConfigItems;
using Configurall.API.Managers;
using Configurall.Services;
using Configurall.Shared.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Configurall.API.Controllers
{
    [Authorizatation]
    [ApiController]
    [Route(nameof(EntityCatalog))]
    public class EntityCatalogController : ConfigurallBaseController
    {
        public EntityCatalogController(ILogger<ConfigurallBaseController> logger) : base(logger) { }

        [HttpGet(nameof(Get))]
        public async Task<IEnumerable<EntityCatalogDTO>> Get()
        => await Execute((token) => { return ConfigurationMgr.Configurall.GetEntityCatalog(); });

        [HttpPut(nameof(Set))]
        public async Task<bool> Set(EntityCatalogDTO entityCatalogDTO)
        => await ExecuteSave((token) => { return ConfigurationMgr.Configurall.SetEntityCatalog(entityCatalogDTO); });

        [HttpPut(nameof(Delete))]
        public async Task<bool> Delete(DeleteEntityCatalogDTO entityCatalogDTO)
        => await ExecuteSave((token) => { return ConfigurationMgr.Configurall.DeleteEntityCatalog(entityCatalogDTO); });
    }


}

﻿using Microsoft.Extensions.Logging;

namespace Configurall.API.Managers
{
    public static class LogMgr
    {
        static ILoggerFactory loggerFactory = null;
        public static ILogger<T> Create<T>()
        {
            return loggerFactory.CreateLogger<T>();
        }

        static LogMgr()         
        {
            loggerFactory = LoggerFactory.Create(builder =>
            {
                builder
                    .AddFilter("Microsoft", LogLevel.Warning)
                    .AddFilter("System", LogLevel.Warning)
                    .AddFilter("LoggingConsoleApp.Program", LogLevel.Debug)
                    .AddConsole();
            });            
        }
    }
}

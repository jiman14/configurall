﻿using Configurall.API.Classes.Utils;
using Configurall.API.ConfigItems;
using Configurall.Shared.DTOs;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Configurall.API.Managers
{

    [System.Serializable]
    public partial class ConfigManager
    {
        ILogger<ConfigManager> log;

        #region Properties

        public ConcurrentDictionary<string, Domain> Domains { get; set; } = new ConcurrentDictionary<string, Domain>();
        public ConcurrentDictionary<string, GroupCategory> GroupsCategories { get; set; } = new ConcurrentDictionary<string, GroupCategory>();
        public ConcurrentDictionary<string, Entity> Entities { get; set; } = new ConcurrentDictionary<string, Entity>();
        public ConcurrentDictionary<string, Catalog> Catalogs { get; set; } = new ConcurrentDictionary<string, Catalog>();
        public ConcurrentDictionary<Guid, EntityCatalog> EntityCatalogs { get; set; } = new ConcurrentDictionary<Guid, EntityCatalog>();
        public ConcurrentDictionary<Guid, Configuration> Configurations { get; set; } = new ConcurrentDictionary<Guid, Configuration>();

        #endregion

        public ConfigManager() => log = LogMgr.Create<ConfigManager>();

        /// <summary>
        /// Get config by row, group and entity
        /// </summary>
        /// <param name="configDTO"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        private Configuration ConfigBase(ConfigDTO configDTO)        
        => Configurations.Values.FirstOrDefault(c => c.RowId == configDTO.RowId && c.GroupName == configDTO.GroupName && c.EntityName == configDTO.EntityName);

        private IEnumerable<Configuration> ConfigEntityBase(string entityName, string rowId)
        => Configurations.Values.Where(c => c.RowId == rowId && c.EntityName == entityName);        

        public bool SetConfig(string token, SetConfigDTO configDTO)
        {
            var conf = ConfigBase(configDTO);
            if (conf != null)
                conf.Content = configDTO.Content;
            else {
                conf = new Configuration
                {
                    RowId = configDTO.RowId,
                    GroupName = configDTO.GroupName,
                    EntityName = configDTO.EntityName,
                    Content = configDTO.Content
                };
                Configurations.TryAdd(conf.Id, conf);
            }
            return true;
        }

        public Configuration GetConfig(string token, ConfigDTO configDTO)
        {
            var conf = ConfigBase(configDTO);
            if (conf == null)
            {
                conf = new Configuration
                {
                    RowId = configDTO.RowId,
                    GroupName = configDTO.GroupName,
                    EntityName = configDTO.EntityName
                };
                Configurations.TryAdd(conf.Id, conf);
            }
            var entity = Entities[configDTO.EntityName];

            var jContent = string.IsNullOrEmpty(entity.Schema) ? null :
                        JsonConvert.DeserializeObject<JObject>(conf.Content);

            var group = GroupsCategories[configDTO.CategoryName].Groups[configDTO.GroupName];
            foreach (var prop in jContent.Properties())
                prop.Value = GetPropertyFromParent(token, configDTO.RowId, group, entity, prop)?.Value;

            conf.Content = JsonConvert.SerializeObject(jContent);
            return conf;
        }

        private JProperty GetPropertyFromParent(string token, string rowId, Group group, Entity entity, JProperty prop)
        {
            // Get value from property (default value modified)
            if (prop.Value.Type != JTokenType.Null || entity.Inherits == null)
                return prop;

            // Get value from parent entity (and parent group)
            foreach (var parentEntityName in entity.Inherits)
            {
                var confs = ConfigEntityBase(parentEntityName, rowId);
                if (confs == null || confs.Count() == 0)
                    continue;

                var entityP = Entities[parentEntityName];

                var conf = confs.FirstOrDefault();
                if (group.Inherits != null && group.Inherits.Count() > 0)
                    conf = confs.Where(c => group.Inherits.Any(g => g.Contains(c.GroupName)))?.FirstOrDefault();

                var jContent = string.IsNullOrEmpty(conf.Content) ?
                    JsonConvert.DeserializeObject<JObject>(entityP.Schema) :
                    JsonConvert.DeserializeObject<JObject>(conf.Content);
                if (jContent != null && jContent.Property(prop.Name) != null)
                    return GetPropertyFromParent(token, rowId, group, entityP, jContent.Property(prop.Name));
            }

            // Get default value from entity schema
            var jSchemaContent = string.IsNullOrEmpty(entity.Schema)? null:
                JsonConvert.DeserializeObject<JObject>(entity.Schema);                    
            if (jSchemaContent != null && jSchemaContent.Property(prop.Name) != null)
                return jSchemaContent.Property(prop.Name);

            return null;
        }

        /// <summary>
        /// Get config by group 
        /// </summary>
        /// <param name="categoryGroupDTO"></param>
        /// <returns></returns>
        public IEnumerable<ConfigResDTO> GetConfigAndEntities(GroupCategoryDTO categoryGroupDTO)
        {
            var rowsIds = GetGroupsRows(
                new GroupDTO { 
                    CategoryName = categoryGroupDTO.CategoryName, 
                    Name = categoryGroupDTO.GroupName 
                });
            
            var conf = Configurations.Values
                .Where(c => rowsIds.Contains(c.RowId))
                .Select(c =>
                {
                    var entity = Entities[c.EntityName];
                    return new ConfigResDTO
                    {
                        RowId = c.RowId,
                        CategoryName = c.CategoryName,
                        GroupName = c.GroupName,
                        EntityName = c.EntityName,
                        EntityInherits = entity?.Inherits,
                        EntitySchema = entity?.Schema,
                        Content = c.Content,
                        RowDomains = c.Domains,
                        EntityCatalogs = EntityCatalogs.Values
                            .Where(ec => ec.EntityName == c.EntityName)
                            .Select(ec => new ECatalogDTO
                            {
                                EntityPropertyName = ec.EntityPropertyName,
                                Catalog = Catalogs[ec.CatalogName].Content.ToObject<IEnumerable<string>>()
                            })
                    };
                });

            return conf;
        }

        public bool DeleteConfig(string token, ConfigDTO configDTO)
        {
            var conf = ConfigBase(configDTO);
            if (conf != null)
                Configurations.TryRemove(conf.Id, out Configuration removedConf);

            return true;
        }

        private void DomainFilter()
        {
            //c.Domains.All(d => RoleDomains.ActiveDomains.Get(token).Contains(Domains.Get(d))));
        }
    }
}

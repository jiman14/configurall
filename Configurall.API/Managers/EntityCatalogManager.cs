﻿using Configurall.API.Classes.Utils;
using Configurall.API.ConfigItems;
using Configurall.Shared.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Configurall.API.Managers
{
    public partial class ConfigManager
    {
        public IEnumerable<EntityCatalogDTO> GetEntityCatalog()
            => EntityCatalogs.Values.Select(ec => new EntityCatalogDTO { CatalogName = ec.CatalogName, EntityName = ec.EntityName, EntityPropertyName = ec.EntityPropertyName, CatalogJsonPath = ec.CatalogJsonPath });

        public bool SetEntityCatalog(EntityCatalogDTO entityCatalogDTO)
            => EntityCatalogs.Get(GetEntityCatalogGuid(entityCatalogDTO.EntityName, entityCatalogDTO.EntityPropertyName, entityCatalogDTO.CatalogName)?? new EntityCatalog().Guid)
            .Set(entityCatalogDTO.EntityName, entityCatalogDTO.EntityPropertyName, entityCatalogDTO.CatalogName, entityCatalogDTO.CatalogJsonPath);

        public bool DeleteEntityCatalog(DeleteEntityCatalogDTO entityCatalogDTO)
            => EntityCatalogs.Remove(GetEntityCatalogGuid(entityCatalogDTO.EntityName, entityCatalogDTO.EntityPropertyName, entityCatalogDTO.CatalogName)??Guid.Empty, out EntityCatalog _);
        

        private Guid? GetEntityCatalogGuid(string entity, string entityProperty, string catalog)
            => EntityCatalogs.Values.FirstOrDefault(ec => ec.EntityName == entity && ec.EntityPropertyName == entityProperty && ec.CatalogName == catalog)?.Guid;

    }
}

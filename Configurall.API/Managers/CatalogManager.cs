﻿using Configurall.API.Classes.Utils;
using Configurall.API.ConfigItems;
using Configurall.Shared.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace Configurall.API.Managers
{
    public partial class ConfigManager
    {
        public bool SetCatalog(CatalogDTO catalogDTO)
        => Catalogs.Get(catalogDTO.Name).Set(catalogDTO.Content, catalogDTO.Schema);

    }
}

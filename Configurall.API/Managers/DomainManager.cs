﻿using Configurall.API.Classes.Utils;
using Configurall.API.ConfigItems;
using Configurall.Shared.DTOs;
using System.Collections.Generic;

namespace Configurall.API.Managers
{
    public partial class ConfigManager
    {
        public void SetDomain(DomainDTO domainDTO)
        => Domains.Get(domainDTO.Name).SetMandatory(domainDTO.Mandatory).Set(domainDTO.Values);

        public bool SetActiveDomain(string token, ActiveDomainDTO activeDomainDTO)
        {
            if (!Domains.ContainsKey(activeDomainDTO.Name))
                return false;

            return RoleDomains.ActiveDomains.Get(token).TryAdd(activeDomainDTO.Name, activeDomainDTO.Value);
        }
    }
}

﻿using Configurall.API.Classes.Utils;
using Configurall.API.ConfigItems;
using Configurall.Shared.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace Configurall.API.Managers
{
    public partial class ConfigManager
    {
        public bool SetEntity(EntityDTO entityDTO)
        => Entities.Get(entityDTO.Name).Set(entityDTO.Schema, entityDTO.Inherits);

        public IEnumerable<EntityDTO> GetEntities()
        => Entities.Values.Select(e => new EntityDTO { Name = e.Name, Inherits = e.Inherits, Schema = e.Schema });
        
    }
}

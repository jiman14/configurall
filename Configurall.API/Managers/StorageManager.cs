﻿using System.IO;
using System.Net.Http;
using System;
using Configurall.API.ConfigItems;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using Configurall.Model;

namespace Configurall.API.Managers
{
    public class ConfigurationMgr
    {
        private static ILogger<ConfigurationMgr> Logger { get; set; } = LogMgr.Create<ConfigurationMgr>();
        private static DBManager BDMgr { get; set; } = new DBManager();
        private static DB_configurall DBConf { get; set; }

        public static ConfigManager Configurall = new ConfigManager();

        public static void Init() 
        {
            _ = LoadConfigurall();
        }

        public async static Task<bool> LoadConfigurall(bool force = false)
        {
            if (Configurall.Domains.Count > 0 && !force)
                return true;

            if (BDMgr == null)
                return false;

            DBConf = await BDMgr.GetConfig();

            try
            {
                Configurall = JsonConvert.DeserializeObject<ConfigManager>(DBConf.Config);
                return true;
            }
            catch (Exception exc)
            {
                Logger.LogError(exc.Message);
                Configurall = new ConfigManager();
                return false;
            }
        }

        public async static Task<bool> Reload()
        {
            await LoadConfigurall(force: true);
            return true;
        }

        public static bool SaveConfigurall()
        {
            try
            {
                DBConf.Config = JsonConvert.SerializeObject(Configurall);

                return BDMgr.UpdateConfig(DBConf);
            }
            catch (Exception exc)
            {
                Logger.LogError(exc.Message);
                return false;
            }
        }

    }
}
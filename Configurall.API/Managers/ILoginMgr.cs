﻿using Configurall.API.Classes;
using Microsoft.Extensions.Configuration;
using System.Configuration;

namespace Configurall.API.Managers
{
    public interface ILoginMgr
    {
        bool CheckAPIKey(string apiKey);
    }
    public class LoginMgr : ILoginMgr
    {
        public LoginMgr()
        {
        }

        public bool CheckAPIKey(string apiKey)
        {
            return apiKey == GlobalVars.APIKey;
        }
    }
}
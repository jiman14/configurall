﻿using Configurall.Model;
using System.Linq;
using System.Threading.Tasks;
using Titania.DBCore;

namespace Configurall.API.Managers
{
    public class DBManager
    {
        public async Task<DB_configurall> GetConfig()
        {
            await Task.Delay(1);

            var config = SQLOperation.NonTransaction<DB_configurall>(c =>
            {
                return c.GetAll<DB_configurall>().FirstOrDefault();
            });

            if (config == null)
            {
                config = new DB_configurall {  };
                CreateConfig(config);
            }
            return config;
        }

        public bool UpdateConfig(DB_configurall config)
        {
            return SQLOperation.Transaction<bool>(t =>
            {
                return t.Update(config);
            });
        }

        private long CreateConfig(DB_configurall config)
        {
            return SQLOperation.Transaction<long>(t =>
            {
                return t.Insert(config);
            });
        }
    }
}

﻿using Configurall.API.Classes.Utils;
using Configurall.API.ConfigItems;
using Configurall.Shared.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace Configurall.API.Managers
{
    public partial class ConfigManager
    {
        private const string CategoryGroupSeparator = ":";

        public IEnumerable<string> GetGroupsNames(CategoryDTO categoryDTO)
        {
            if (GroupsCategories.ContainsKey(categoryDTO.Name))
                return GroupsCategories[categoryDTO.Name].Groups.Keys;

            return new string[] { };
        }

        public IEnumerable<string> GetGroupsRows(GroupDTO groupDTO)
            => Configurations.Values
                .Where(c => c.GroupName == groupDTO.Name)
                .Select(c => c.RowId);
        

        public IEnumerable<GroupDTO> GetGroups(CategoryDTO category)
        => GroupsCategories.ContainsKey(category.Name)?
                GroupsCategories[category.Name].Groups.Values.Select(g => new GroupDTO {
                    Name = g.Name,
                    Inherits = g.Inherits,
                    CategoryName = category.Name
                }):
                new GroupDTO[] { };
        

        public IEnumerable<string> GetParentGroupsRowIds(GroupCategoryDTO categoryGroupDTO)
        {
            var group = GroupsCategories.Get(categoryGroupDTO.CategoryName).Groups.Get(categoryGroupDTO.GroupName);
            if (group.Inherits == null)
                return new string[0];

            List<string> groups = new List<string>();
            foreach (var parentGroupFullName in group.Inherits)
                groups.AddRange(Configurations.Values
                    .Where(c => c.GroupName == (GetParentGroup(parentGroupFullName)?.Name??string.Empty))
                    .Select(c => c.RowId).Distinct());

            return groups;
        }
        internal Group GetParentGroup(string parentGroupFullName)
        {
            if (!parentGroupFullName.Contains(CategoryGroupSeparator))
                return null;

            var categoryAndGroup = parentGroupFullName.Split(CategoryGroupSeparator);
            return GroupsCategories.Get(categoryAndGroup[0]).Groups.Get(categoryAndGroup[1]);
        }

        public IEnumerable<string> GetCategories() => GroupsCategories.Keys;

        public bool SetGroup(GroupDTO groupDTO)
        {
            GroupsCategories.Get(groupDTO.CategoryName).Groups.Get(groupDTO.Name).Set(groupDTO.Inherits);
            return true;
        }

        public bool SetGroupCategory(CategoryDTO categoryDTO)
        => GroupsCategories.Get(categoryDTO.Name) != null;

    }
}

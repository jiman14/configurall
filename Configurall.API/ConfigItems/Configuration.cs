﻿using System;
using System.Collections.Generic;

namespace Configurall.API.ConfigItems
{

    [System.Serializable]
    public class Configuration
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string RowId { get; set; }
        public string CategoryName { get; set; }
        public string GroupName { get; set; }
        public string EntityName { get; set; }
        public Dictionary<string, string> Domains { get; set; } = new Dictionary<string, string>();
        public string Content { get; set; }

    }
}

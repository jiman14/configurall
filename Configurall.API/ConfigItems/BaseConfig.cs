﻿using System;

namespace Configurall.API.ConfigItems
{
    [System.Serializable]
    public class BaseConfig
    {
        public Guid @Guid { get; set; } = System.Guid.NewGuid();
        public string Name { get; set; }

        public BaseConfig(string name) => Name = name;
    }
}

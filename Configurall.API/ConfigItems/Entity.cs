﻿using System;
using System.Collections.Generic;

namespace Configurall.API.ConfigItems
{
    [Serializable]
    public class Entity : BaseConfig
    {
        public IEnumerable<string> Inherits { get; set; } = new string[0];
        public string Schema { get; set; }
        public Entity(string name) : base(name) { }

        public bool Set(string schema, IEnumerable<string> inherits)
        {
            Schema = schema;
            Inherits = inherits;

            return true;
        }
    }
}

﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Configurall.API.ConfigItems
{
    [Serializable]
    public class Catalog : BaseConfig
    {
        public string Schema { get; set; }
        public JToken Content { get; set; }
        public Catalog(string name) : base(name) { }
        public bool Set(string content, string schema)
        {
            Content = JsonConvert.DeserializeObject<JToken>(content);
            Schema = schema;

            return true;
        }
    }
}

﻿using Configurall.API.Classes.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Configurall.API.ConfigItems
{
    [Serializable]
    public class Group: BaseConfig
    {
        public IEnumerable<string> Inherits { get; set; } = new string[0];
        public Group(string name) : base(name) {}

        public void Set(IEnumerable<string> inherits) => Inherits = inherits;        
    }
}

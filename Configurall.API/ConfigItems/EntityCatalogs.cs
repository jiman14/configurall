﻿using System;

namespace Configurall.API.ConfigItems
{
    [Serializable]
    public class EntityCatalog : BaseConfig
    {
        public string EntityName { get; set; }
        public string EntityPropertyName { get; set; }
        public string CatalogName { get; set; }
        public string CatalogJsonPath { get; set; }
        public EntityCatalog() : base(string.Empty) { }
        
        public bool Set(string entityName, string entityPropertyName, string catalogName, string catalogJsonPath)
        {
            EntityName = entityName;
            EntityPropertyName = entityPropertyName;
            CatalogName = catalogName;
            CatalogJsonPath = catalogJsonPath;

            return true;
        }
    }
}

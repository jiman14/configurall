﻿using Configurall.API.Classes.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Configurall.API.ConfigItems
{
    [Serializable]
    public class GroupCategory: BaseConfig
    {
        public ConcurrentDictionary<string, Group> Groups {get; set;} = new ConcurrentDictionary<string, Group>();
        public GroupCategory(string name) : base(name) {}

        public void TryAddGroup(string groupName) => Groups.Get(groupName);
    }
}

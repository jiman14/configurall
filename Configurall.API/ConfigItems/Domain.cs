﻿using System;
using System.Collections.Generic;

namespace Configurall.API.ConfigItems
{
    [Serializable]
    public class Domain : BaseConfig
    {
        public string Value { get; set; }
        public bool Mandatory { get; set; } = false;
        public IEnumerable<string> Values { get; set; } = new string[0];
        public Domain(string name) : base(name) { }

        public Domain SetMandatory(bool mandatory)
        {
            Mandatory = mandatory;
            return this;
        }

        public void Set(IEnumerable<string> values)
        => Values = values;
    }
}

﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Configurall.API.ConfigItems
{
    public static class RoleDomains
    {
        /// <summary>
        /// Concurrent dictionary by role, that contains a dictionary by domain name with domain active value, for example: Role: role1 -> [{Language: en}, {Devicype: Android}]
        /// </summary>
        public static ConcurrentDictionary<string, Dictionary<string, string>> ActiveDomains { get; set; } = new ConcurrentDictionary<string, Dictionary<string, string>>();


    }
}

﻿using Configurall.API.Classes;
using Configurall.API.Managers;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Configurall.Services
{
    public class SrvAccess_Basic : ISrvAccess
    {
        private readonly ILogger _logger = LogMgr.Create<SrvAccess_Basic>();
        private ILoginMgr _login;
        public bool IsActive() => true;

        public SrvAccess_Basic(ILoginMgr login) => _login = login;        

        public string Login(string apiKey)
        {
            if (!_login.CheckAPIKey(apiKey))
                return null;

            var token = Base64UrlEncoder.Encode(generateJwtToken(apiKey));

            return token;
        }

        public void SetAuthentication(AuthenticationBuilder authenticationBuilder)
        {
            authenticationBuilder.AddJwtBearer(options =>
            {
            });
        }
        private string generateJwtToken(string apiKey)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(GlobalVars.BasicAuthSecret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { 
                    new Claim(ClaimTypesEnum.NameIdentifier.ToString(), apiKey)
                }),
                Expires = DateTime.UtcNow.AddDays(GlobalVars.BasicAuthTokenExpirationInDays),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public bool CheckToken(AuthorizationFilterContext context)
        {
            var token = context.HttpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            if (token == null)
                return false;
            
            return attachUserToContext(context, Base64UrlEncoder.Decode(token));
        }
        private bool attachUserToContext(AuthorizationFilterContext context, string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(GlobalVars.BasicAuthSecret);
                var principal = tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                
                if (principal.HasClaim(c => c.Type == ClaimTypesEnum.NameIdentifier.ToString()))
                {
                    // attach user to context on successful jwt validation
                    //context.HttpContext.Items["User"] = new LoginMgr().GetUserById(userId);

                    //authorizing the http context 
                    context.HttpContext.User.AddIdentity((ClaimsIdentity)principal.Identity);
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, context);
                return false;
            }
        }

    }


}

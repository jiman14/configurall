﻿using Configurall.API.Managers;
using Configurall.Model;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Configurall.Services
{
    public enum ClaimTypesEnum
    {
        NameIdentifier,
        Email,
        Role
    }

    public interface ISrvAccess
    {
        bool IsActive();

        void SetAuthentication(AuthenticationBuilder authenticationBuilder);
        
        string Login(string apiKey);
        
        bool CheckToken(AuthorizationFilterContext context);
    }

    public static class SrvAccess
    {
        public static ISrvAccess Instance { get; private set; }

        static SrvAccess()
        {
            Instance = new SrvAccess_Basic(new LoginMgr());
        }

        /// <summary>
        /// Get User claims dictionary. Obtain Key from ClaimTypes
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string GetUserClaims(ClaimsPrincipal user, ClaimTypesEnum claimType)
        {
            return user.Claims.FirstOrDefault(c => c.Type == claimType.ToString()).Value;
        }
    }


    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizatationAttribute : System.Attribute, IAuthorizationFilter
    {

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (!SrvAccess.Instance.CheckToken(context))
                context.Result = new JsonResult("Unauthorized")
                {
                    StatusCode = (int)HttpStatusCode.Forbidden
                };
        }
    }
}
